﻿namespace Bravo.FeatureFlags.Domain.Customer
{
	using Bravo.Infrastructure.Common.Domain;

	public interface ICustomerRepository : IRepository<Customer, string>
	{
		/// <summary>
		/// Get customer details that contains related entities.
		/// </summary>
		CustomerDetails GetDetails(string id);

		/// <summary>
		/// Gets enable feature flags of a specific customer.
		/// </summary>
		CustomerFeatureFlags GetCustomerFeatureFlags(string id);

		/// <summary>
		/// Updates the customer feature flags.
		/// </summary>
		void UpdateCustomerFeatureFlags(CustomerFeatureFlags customerFeatureFlags);
	}
}
