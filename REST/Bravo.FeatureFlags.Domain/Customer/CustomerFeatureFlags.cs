﻿namespace Bravo.FeatureFlags.Domain.Customer
{
	using Bravo.FeatureFlags.Domain.FeatureFlag;
	using Bravo.Infrastructure.Common.Domain;
	using System;
	using System.Collections.Generic;

	public class CustomerFeatureFlags : EntityBase<string>, IAggregateRoot
	{
		#region Properties

		/// <summary>
		/// Gets or sets customer.
		/// </summary>
		public Customer Customer { get; set; }

		/// <summary>
		/// Gets or sets list of enable feature flags for specific customer and version.
		/// </summary>
		public IEnumerable<FeatureFlag> EnableFeatureFlags { get; set; }

		#endregion

		#region Implementations

		public override void Validate()
		{
			if (string.IsNullOrWhiteSpace(this.ID))
			{
				this.AddBrokenRule(CustomerFeatureFlagsBusinessRule.CustomerFeatureFlagsIDRequired);
			}

			if (this.Customer == null)
			{
				this.AddBrokenRule(CustomerFeatureFlagsBusinessRule.CustomerInCustomerFeatureFlagsRequired);
			}

			foreach (var enableFeatureFlag in this.EnableFeatureFlags)
			{
				if (enableFeatureFlag.ID == Guid.Empty)
				{
					this.AddBrokenRule(FeatureFlagBusinessRule.FeatureFlagIDRequired);
				}
			}
		}

		#endregion
	}
}
