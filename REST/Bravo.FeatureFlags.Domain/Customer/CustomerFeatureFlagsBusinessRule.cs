﻿namespace Bravo.FeatureFlags.Domain.Customer
{
	using Bravo.Infrastructure.Common.Domain;

	public static class CustomerFeatureFlagsBusinessRule
	{
		/// <summary>
		/// Business rule of customer feature flags ID required.
		/// </summary>
		public static readonly BusinessRule CustomerFeatureFlagsIDRequired = new BusinessRule("A customer feature flags must has an ID.");

		/// <summary>
		/// Business rule of customer required.
		/// </summary>
		public static readonly BusinessRule CustomerInCustomerFeatureFlagsRequired = new BusinessRule("A customer feature flags must has a customer.");
	}
}
