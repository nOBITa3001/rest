﻿namespace Bravo.FeatureFlags.Domain.Customer
{
	using Bravo.Infrastructure.Common.Domain;

	public static class CustomerBusinessRule
	{
		/// <summary>
		/// Business rule of customer ID required.
		/// </summary>
		public static readonly BusinessRule CustomerIDRequired = new BusinessRule("A customer must has an ID.");
	}
}
