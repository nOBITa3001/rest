﻿namespace Bravo.FeatureFlags.Domain.Customer
{
	using Bravo.Infrastructure.Common.Domain;
	using System;

	public class Customer : EntityBase<string>, IAggregateRoot
	{
		#region Implementations

		public override void Validate()
		{
			if (string.IsNullOrWhiteSpace(this.ID))
			{
				this.AddBrokenRule(CustomerBusinessRule.CustomerIDRequired);
			}

			if (string.IsNullOrWhiteSpace(this.RecCreatedBy))
			{
				this.AddBrokenRule(EntityBusinessRule.RecordCreatedByRequired);
			}

			if (this.RecCreatedWhen == DateTime.MinValue)
			{
				this.AddBrokenRule(EntityBusinessRule.RecordCreatedWhenRequired);
			}
		}

		#endregion
	}
}
