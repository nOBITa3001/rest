﻿namespace Bravo.FeatureFlags.Domain.Customer
{
	using FeatureFlag;
	using System.Collections.Generic;

	/// <summary>
	/// Customer details class that contains related entities.
	/// </summary>
	public class CustomerDetails : Customer
	{
		#region Properties

		/// <summary>
		/// Gets or sets enable feature flags.
		/// </summary>
		public IEnumerable<FeatureFlag> EnableFeatureFlags { get; set; }

		#endregion
	}
}
