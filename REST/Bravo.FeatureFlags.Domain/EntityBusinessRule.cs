﻿namespace Bravo.FeatureFlags.Domain
{
	using Bravo.Infrastructure.Common.Domain;

	public static class EntityBusinessRule
	{
		/// <summary>
		/// Business rule of entity record created by required.
		/// </summary>
		public static readonly BusinessRule RecordCreatedByRequired = new BusinessRule("An entity must has a record created by");

		/// <summary>
		/// Business rule of entity record created when required.
		/// </summary>
		public static readonly BusinessRule RecordCreatedWhenRequired = new BusinessRule("An entity must has a record created when");
	}
}
