﻿namespace Bravo.FeatureFlags.Domain.FeatureFlag
{
	using Bravo.Infrastructure.Common.Domain;

	public static class FeatureFlagCustomersBusinessRule
	{
		/// <summary>
		/// Business rule of feature flag customers ID required.
		/// </summary>
		public static readonly BusinessRule FeatureFlagCustomersIDRequired = new BusinessRule("A feature flag customers must has an ID.");

		/// <summary>
		/// Business rule of feature flag required.
		/// </summary>
		public static readonly BusinessRule FeatureFlagInFeatureFlagCustomersRequired = new BusinessRule("A feature flag customers must has a feature flag.");
	}
}
