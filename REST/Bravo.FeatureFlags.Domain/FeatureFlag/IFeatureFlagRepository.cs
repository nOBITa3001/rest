﻿namespace Bravo.FeatureFlags.Domain.FeatureFlag
{
	using Bravo.Infrastructure.Common.Domain;
	using System;
	using System.Collections.Generic;

	public interface IFeatureFlagRepository : IRepository<FeatureFlag, Guid>
	{
		/// <summary>
		/// Get feature flag details that contains related entities.
		/// </summary>
		FeatureFlagDetails GetDetails(Guid id);

		/// <summary>
		/// Finds enable feature flags for specific customer.
		/// </summary>
		[Obsolete]
		IEnumerable<FeatureFlag> FindEnableFeatureFlags(string customerId);

		/// <summary>
		/// Finds enable feature flags for specific customer and application version.
		/// </summary>
		[Obsolete]
		IEnumerable<FeatureFlag> FindEnableFeatureFlags(string customerId, string appVersion);

		/// <summary>
		/// Determines whether a customer is authorized for a request feature flag or not.
		/// </summary>
		bool IsAuthorizedCustomer(Guid id, string customerId);

		/// <summary>
		/// Determines whether a customer is authorized for a request feature flag or not.
		/// </summary>
		[Obsolete]
		bool IsAuthorizedCustomer(string customerId, string appVersion, string featureFlagName);

		/// <summary>
		/// Gets enable customers of a specific feature flag.
		/// </summary>
		FeatureFlagCustomers GetFeatureFlagCustomers(Guid id);

		/// <summary>
		/// Update feature flag customers.
		/// </summary>
		void UpdateFeatureFlagCustomers(FeatureFlagCustomers featureFlagCustomers);
	}
}
