﻿namespace Bravo.FeatureFlags.Domain.FeatureFlag
{
	using Bravo.Infrastructure.Common.Domain;
	using System;

	/// <summary>
	/// Feature flag class.
	/// </summary>
	public class FeatureFlag : EntityBase<Guid>, IAggregateRoot
	{
		#region Properties

		/// <summary>
		/// Gets or sets feature flag name.
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets feature flag description.
		/// </summary>
		public string Description { get; set; }

		#endregion

		#region Implementations

		public override void Validate()
		{
			if (this.ID == Guid.Empty)
			{
				this.AddBrokenRule(FeatureFlagBusinessRule.FeatureFlagIDRequired);
			}

			if (string.IsNullOrWhiteSpace(this.Name))
			{
				this.AddBrokenRule(FeatureFlagBusinessRule.FeatureFlagNameRequired);
			}

			if (this.IsDescriptionLengthExceededLimit())
			{
				this.AddBrokenRule(FeatureFlagBusinessRule.FeatureFlagDescriptionLengthExceededLimit);
			}

			if (string.IsNullOrWhiteSpace(this.RecCreatedBy))
			{
				this.AddBrokenRule(EntityBusinessRule.RecordCreatedByRequired);
			}

			if (this.RecCreatedWhen == DateTime.MinValue)
			{
				this.AddBrokenRule(EntityBusinessRule.RecordCreatedWhenRequired);
			}
		}

		#endregion

		#region Methods

		private bool IsDescriptionLengthExceededLimit()
		{
			return (!string.IsNullOrWhiteSpace(this.Description)
					&& this.Description.Length > 500);
		}

		#endregion
	}
}
