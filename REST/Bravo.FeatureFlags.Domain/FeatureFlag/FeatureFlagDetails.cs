﻿namespace Bravo.FeatureFlags.Domain.FeatureFlag
{
	using Bravo.FeatureFlags.Domain.Customer;
	using System.Collections.Generic;

	/// <summary>
	/// Feature flag details class that contains related entities.
	/// </summary>
	public class FeatureFlagDetails : FeatureFlag
	{
		#region Properties

		/// <summary>
		/// Gets or sets support application versions.
		/// </summary>
		public IEnumerable<string> SupportAppVersions { get; set; }

		/// <summary>
		/// Gets or sets enable customers.
		/// </summary>
		public IEnumerable<Customer> EnableCustomers { get; set; }

		#endregion
	}
}
