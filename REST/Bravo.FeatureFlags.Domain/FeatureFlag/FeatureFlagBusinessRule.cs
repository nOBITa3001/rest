﻿namespace Bravo.FeatureFlags.Domain.FeatureFlag
{
	using Bravo.Infrastructure.Common.Domain;

	public class FeatureFlagBusinessRule
	{
		/// <summary>
		/// Business rule of feature flag ID required.
		/// </summary>
		public static readonly BusinessRule FeatureFlagIDRequired = new BusinessRule("A feature flag must has an ID.");

		/// <summary>
		/// Business rule of feature flag name required.
		/// </summary>
		public static readonly BusinessRule FeatureFlagNameRequired = new BusinessRule("A feature flag must has a name.");

		/// <summary>
		/// Business rule of feature flag name required.
		/// </summary>
		public static readonly BusinessRule FeatureFlagDescriptionLengthExceededLimit = new BusinessRule("A feature flag description must be less than 500 characters.");
	}
}
