﻿namespace Bravo.FeatureFlags.Domain.FeatureFlag
{
	using Bravo.FeatureFlags.Domain.Customer;
	using Bravo.Infrastructure.Common.Domain;
	using System;
	using System.Collections.Generic;

	public class FeatureFlagCustomers : EntityBase<Guid>, IAggregateRoot
	{
		#region Properties

		/// <summary>
		/// Gets or sets feature flag.
		/// </summary>
		public FeatureFlag FeatureFlag { get; set; }

		/// <summary>
		/// Gets or sets enable customers.
		/// </summary>
		public IEnumerable<Customer> EnableCustomers { get; set; }

		#endregion

		#region Implementations

		public override void Validate()
		{
			if (this.ID == Guid.Empty)
			{
				this.AddBrokenRule(FeatureFlagCustomersBusinessRule.FeatureFlagCustomersIDRequired);
			}

			if (this.FeatureFlag == null)
			{
				this.AddBrokenRule(FeatureFlagCustomersBusinessRule.FeatureFlagInFeatureFlagCustomersRequired);
			}

			foreach (var enableCustomer in this.EnableCustomers)
			{
				if (string.IsNullOrWhiteSpace(enableCustomer.ID))
				{
					this.AddBrokenRule(CustomerBusinessRule.CustomerIDRequired);
				}
			}
		}

		#endregion
	}
}
