﻿namespace Bravo.FeatureFlags.Repository.EF.Models
{
	using System;
	using System.ComponentModel.DataAnnotations;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations.Schema;

	public class FeatureFlag : ModelBase
	{
		/// <summary>
		/// Gets or sets feature flag ID.
		/// </summary>
		public Guid ID { get; set; }

		/// <summary>
		/// Gets or sets feature flag name.
		/// </summary>
		[Required]
		[StringLength(100)]
		[Index("IX_Name", 1, IsUnique = true)]
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets feature flag description.
		/// </summary>
		[StringLength(500)]
		public string Description { get; set; }

		public ICollection<FeatureFlagCustomer> EnableCustomers { get; set; }
	}
}
