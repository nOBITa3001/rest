﻿namespace Bravo.FeatureFlags.Repository.EF.Models
{
	using System.Collections.Generic;

	public class Customer : ModelBase
	{
		/// <summary>
		/// Gets or sets customer ID.
		/// </summary>
		public string ID { get; set; }

		public ICollection<FeatureFlagCustomer> EnableFeatureFlags { get; set; }
	}
}
