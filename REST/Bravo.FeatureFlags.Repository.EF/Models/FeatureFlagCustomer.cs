﻿namespace Bravo.FeatureFlags.Repository.EF.Models
{
	using System;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;

	public class FeatureFlagCustomer
	{
		/// <summary>
		/// Gets or sets feature flag ID.
		/// </summary>
		[Key, Column(Order = 0), ForeignKey("FeatureFlag")]
		public Guid FeatureFlagID { get; set; }

		/// <summary>
		/// Gets or sets customer ID.
		/// </summary>
		[Key, Column(Order = 1), ForeignKey("Customer")]
		public string CustomerID { get; set; }

		/// <summary>
		/// Gets or sets navigation property of feature flag.
		/// </summary>
		public FeatureFlag FeatureFlag { get; set; }

		/// <summary>
		/// Gets or sets navigation property of customers.
		/// </summary>
		public Customer Customer { get; set; }
	}
}
