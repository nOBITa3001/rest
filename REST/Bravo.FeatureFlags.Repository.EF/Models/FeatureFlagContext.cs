﻿namespace Bravo.FeatureFlags.Repository.EF.Models
{
	using System.Data.Entity;
	using System.Data.Entity.ModelConfiguration.Conventions;

	public class FeatureFlagContext : DbContext
	{
		#region Properties

		public DbSet<FeatureFlag> FeatureFlags { get; set; }
		public DbSet<Customer> Customers { get; set; }
		public DbSet<FeatureFlagCustomer> FeatureFlagCustomers { get; set; }

		#endregion

		#region Constructors

		public FeatureFlagContext()
			: base("FeatureFlagContext") { }

		#endregion

		#region Implementations

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
		}

		#endregion
	}
}
