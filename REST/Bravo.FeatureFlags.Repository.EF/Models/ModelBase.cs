﻿namespace Bravo.FeatureFlags.Repository.EF.Models
{
	using System;
	using System.ComponentModel.DataAnnotations;

	public class ModelBase
	{
		/// <summary>
		/// Gets or sets record status.
		/// </summary>
		[Required]
		public int RecStatus { get; set; }

		/// <summary>
		/// Gets or sets record created by.
		/// </summary>
		[Required]
		[StringLength(256)]
		public string RecCreatedBy { get; set; }

		/// <summary>
		/// Gets or sets record created when.
		/// </summary>
		[Required]
		public DateTime RecCreatedWhen { get; set; }

		/// <summary>
		/// Gets or sets record modified by.
		/// </summary>
		[StringLength(256)]
		public string RecModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets record modified when.
		/// </summary>
		public DateTime? RecModifiedWhen { get; set; }

		/// <summary>
		/// Gets or sets record deleted by.
		/// </summary>
		[StringLength(256)]
		public string RecDeletedBy { get; set; }

		/// <summary>
		/// Gets or sets record deleted when.
		/// </summary>
		public DateTime? RecDeletedWhen { get; set; }
	}
}
