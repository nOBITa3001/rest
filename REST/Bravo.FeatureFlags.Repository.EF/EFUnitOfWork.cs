﻿namespace Bravo.FeatureFlags.Repository.EF
{
	using Infrastructure.Common.Domain;
	using Infrastructure.Common.UnitOfWork;
	using System.Collections.Generic;
	using System.Transactions;

	// TODO: Shoule move to infrastructure ?
	public class EFUnitOfWork : IUnitOfWork
	{
		#region Declarations

		private readonly Dictionary<IAggregateRoot, IUnitOfWorkRepository> insertedAggregates;
		private readonly Dictionary<IAggregateRoot, IUnitOfWorkRepository> updatedAggregates;
		private readonly Dictionary<IAggregateRoot, IUnitOfWorkRepository> deletedAggregates;

		#endregion

		#region Constructors

		public EFUnitOfWork()
		{
			this.insertedAggregates = new Dictionary<IAggregateRoot, IUnitOfWorkRepository>();
			this.updatedAggregates = new Dictionary<IAggregateRoot, IUnitOfWorkRepository>();
			this.deletedAggregates = new Dictionary<IAggregateRoot, IUnitOfWorkRepository>();
		}

		#endregion

		#region Implementations

		public void RegisterUpdate(IAggregateRoot aggregateRoot, IUnitOfWorkRepository repository)
		{
			if (!this.updatedAggregates.ContainsKey(aggregateRoot))
			{
				this.updatedAggregates.Add(aggregateRoot, repository);
			}
		}

		public void RegisterInsert(IAggregateRoot aggregateRoot, IUnitOfWorkRepository repository)
		{
			if (!this.insertedAggregates.ContainsKey(aggregateRoot))
			{
				this.insertedAggregates.Add(aggregateRoot, repository);
			}
		}

		public void RegisterDelete(IAggregateRoot aggregateRoot, IUnitOfWorkRepository repository)
		{
			if (!this.deletedAggregates.ContainsKey(aggregateRoot))
			{
				this.deletedAggregates.Add(aggregateRoot, repository);
			}
		}

		public void Commit()
		{
			using (TransactionScope scope = new TransactionScope())
			{
				foreach (IAggregateRoot aggregateRoot in this.insertedAggregates.Keys)
				{
					this.insertedAggregates[aggregateRoot].PersistInsert(aggregateRoot);
				}

				foreach (IAggregateRoot aggregateRoot in this.updatedAggregates.Keys)
				{
					this.updatedAggregates[aggregateRoot].PersistUpdate(aggregateRoot);
				}

				foreach (IAggregateRoot aggregateRoot in this.deletedAggregates.Keys)
				{
					this.deletedAggregates[aggregateRoot].PersistDelete(aggregateRoot);
				}

				scope.Complete();
			}
		}

		#endregion
	}
}
