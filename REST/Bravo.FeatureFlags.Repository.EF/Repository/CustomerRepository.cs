﻿namespace Bravo.FeatureFlags.Repository.EF.Repository
{
	using Bravo.Infrastructure.Common.Domain;
	using Bravo.Infrastructure.Common.Enum;
	using Bravo.Infrastructure.Common.UnitOfWork;
	using Domain.Customer;
	using Domain.FeatureFlag;
	using System;
	using System.Collections.Generic;
	using System.Data.Entity;
	using System.Data.Entity.Core;
	using System.Linq;

	public class CustomerRepository : Repository<Customer, string>, ICustomerRepository
	{
		#region Constructors

		public CustomerRepository(IUnitOfWork unitOfWork, IObjectContextFactory objectContextFactory)
			: base(unitOfWork, objectContextFactory) { }

		#endregion

		#region Implementations

		public override Customer Get(string id)
		{
			var result = default(Customer);

			var customer = (from x in this.DbContext.Customers
							where x.ID.Equals(id, StringComparison.InvariantCultureIgnoreCase)
									&& x.RecStatus >= (int)RecordStatus.Active
							select x).SingleOrDefault();
			if (customer != null)
			{
				result = ConvertToDomainType(customer);
			}

			return result;
		}

		public CustomerDetails GetDetails(string id)
		{
			var result = default(CustomerDetails);

			var customer = (from x in this.DbContext.Customers
							.Include(y => y.EnableFeatureFlags.Select(z => z.FeatureFlag))
							where x.ID.Equals(id, StringComparison.InvariantCultureIgnoreCase)
									&& x.RecStatus >= (int)RecordStatus.Active
							select x).SingleOrDefault();
			if (customer != null)
			{
				// TODO: Try to include this condition into query above
				if (customer.EnableFeatureFlags != null && customer.EnableFeatureFlags.Count > 0)
				{
					customer.EnableFeatureFlags = customer.EnableFeatureFlags.Where(x => x.FeatureFlag.RecStatus >= (int)RecordStatus.Active).ToList();
				}
				// End TODO: Try to include this condition into query above
				result = ConvertToCustomerDetailsDomainType(customer, customer.EnableFeatureFlags.Select(x => x.FeatureFlag));
			}

			return result;
		}

		public IEnumerable<Customer> GetAll()
		{
			var result = default(IEnumerable<Customer>);

			var customers = (from x in this.DbContext.Customers
								where x.RecStatus >= (int)RecordStatus.Active
								select x);
			if (customers != null && customers.Any())
			{
				result = ConvertToDomainTypes(customers);
			}

			return result;
		}

		public CustomerFeatureFlags GetCustomerFeatureFlags(string id)
		{
			var result = default(CustomerFeatureFlags);

			var customer = (from x in this.DbContext.Customers
							.Include(y => y.EnableFeatureFlags.Select(z => z.FeatureFlag))
							where x.ID.Equals(id, StringComparison.InvariantCultureIgnoreCase)
									&& x.RecStatus >= (int)RecordStatus.Active
							select x).SingleOrDefault();
			if (customer != null)
			{
				// TODO: Try to include this condition into query above
				if (customer.EnableFeatureFlags != null && customer.EnableFeatureFlags.Count > 0)
				{
					customer.EnableFeatureFlags = customer.EnableFeatureFlags.Where(x => x.FeatureFlag.RecStatus >= (int)RecordStatus.Active).ToList();
				}
				// End TODO: Try to include this condition into query above
				result = this.ConvertToCustomerFeatureFlagsDomainType(customer, customer.EnableFeatureFlags);
			}
			else
			{
				throw new ObjectNotFoundException(string.Concat(id, " does not exist."));
			}

			return result;
		}

		public void UpdateCustomerFeatureFlags(CustomerFeatureFlags customerFeatureFlags)
		{
			this.UnitOfWork.RegisterUpdate(customerFeatureFlags, this);
		}

		public override void PersistInsert(IAggregateRoot aggregateRoot)
		{
			if (aggregateRoot is Customer)
			{
				var newCustomer = (Customer)aggregateRoot;

				this.ThrowExceptionIfNewCustomerIsInvalid(newCustomer);

				this.DbContext.Customers.Add(ConvertToEntityType(newCustomer));
			}

			this.DbContext.SaveChanges();
		}

		public override void PersistUpdate(Infrastructure.Common.Domain.IAggregateRoot aggregateRoot)
		{
			if (aggregateRoot is CustomerFeatureFlags)
			{
				// TODO: Refactor
				var newCustomerFeatureFlags = (CustomerFeatureFlags)aggregateRoot;

				// Removes existing
				var existingCustomerFeatureFlags = (from x in this.DbContext.FeatureFlagCustomers
													where x.CustomerID.Equals(newCustomerFeatureFlags.ID, StringComparison.InvariantCultureIgnoreCase)
													select x).ToList();

				if (existingCustomerFeatureFlags != null && existingCustomerFeatureFlags.Count > 0)
				{
					this.DbContext.FeatureFlagCustomers.RemoveRange(existingCustomerFeatureFlags);
				}

				// Adds new
				foreach (var item in newCustomerFeatureFlags.EnableFeatureFlags)
				{
					this.DbContext.FeatureFlagCustomers.Add(new Models.FeatureFlagCustomer
					{
						FeatureFlagID = item.ID
						, CustomerID = newCustomerFeatureFlags.ID
					});
				}
				// End TODO: Refactor
			}

			this.DbContext.SaveChanges();
		}

		public override void PersistDelete(Infrastructure.Common.Domain.IAggregateRoot aggregateRoot)
		{
			if (aggregateRoot is Customer)
			{
				var deletingCustomerDomain = (Customer)aggregateRoot;

				var existingCustomerEntity = (from x in this.DbContext.Customers
												where x.ID.Equals(deletingCustomerDomain.ID, StringComparison.InvariantCulture)
														&& x.RecStatus >= (int)RecordStatus.Active
												select x).SingleOrDefault();
				if (existingCustomerEntity != null)
				{
					this.AssignSoftDeletedPropertiesToCustomerEntityType(ref existingCustomerEntity, deletingCustomerDomain);
				}
				else
				{
					throw new ObjectNotFoundException(string.Concat(deletingCustomerDomain.ID, " does not exist."));
				}
			}

			this.DbContext.SaveChanges();
		}

		#endregion

		#region Methods

		public static Customer ConvertToDomainType(Models.Customer customerEntity)
		{
			var result = default(Customer);

			if (customerEntity != null)
			{
				result = new Customer
				{
					ID = customerEntity.ID
					, RecStatus = customerEntity.RecStatus
					, RecCreatedBy = customerEntity.RecCreatedBy
					, RecCreatedWhen = customerEntity.RecCreatedWhen
					, RecModifiedBy = customerEntity.RecModifiedBy
					, RecModifiedWhen = customerEntity.RecModifiedWhen
					, RecDeletedBy = customerEntity.RecDeletedBy
					, RecDeletedWhen = customerEntity.RecDeletedWhen
				};
			}

			return result;
		}

		public static IEnumerable<Customer> ConvertToDomainTypes(IEnumerable<Models.Customer> customerEntities)
		{
			var result = default(IEnumerable<Customer>);

			if (customerEntities != null && customerEntities.Any())
			{
				var customers =  new List<Customer>();
				foreach (var customerEntity in customerEntities)
				{
					customers.Add(ConvertToDomainType(customerEntity));
				}
				result = customers;
			}

			return result;
		}

		private CustomerFeatureFlags ConvertToCustomerFeatureFlagsDomainType(Models.Customer customer, IEnumerable<Models.FeatureFlagCustomer> featureFlagCustomerEntities)
		{
			var result = default(CustomerFeatureFlags);

			if (customer != null)
			{
				result = new CustomerFeatureFlags
				{
					ID = customer.ID
					, Customer = ConvertToDomainType(customer)
				};

				if (featureFlagCustomerEntities != null && featureFlagCustomerEntities.Any())
				{
					var featureFlagEntities = featureFlagCustomerEntities.Select(x => x.FeatureFlag);
					result.EnableFeatureFlags = FeatureFlagRepository.ConvertToDomainTypes(featureFlagEntities);
				}
			}

			return result;
		}

		public static Models.Customer ConvertToEntityType(Customer customerDomain)
		{
			var result = default(Models.Customer);

			if (customerDomain != null)
			{
				result = new Models.Customer
				{
					ID = customerDomain.ID
					, RecStatus = customerDomain.RecStatus
					, RecCreatedBy = customerDomain.RecCreatedBy
					, RecCreatedWhen = customerDomain.RecCreatedWhen
					, RecModifiedBy = customerDomain.RecModifiedBy
					, RecModifiedWhen = customerDomain.RecModifiedWhen
					, RecDeletedBy = customerDomain.RecDeletedBy
					, RecDeletedWhen = customerDomain.RecDeletedWhen
				};
			}

			return result;
		}

		private void ThrowExceptionIfNewCustomerIsInvalid(Customer newCustomer)
		{
			var existingCustomer = (from x in this.DbContext.Customers
									where x.ID.Equals(newCustomer.ID, StringComparison.InvariantCultureIgnoreCase)
									select x).SingleOrDefault();
			if (existingCustomer != null)
			{
				throw new ArgumentException(string.Concat(newCustomer.ID, " already exists."));
			}
		}

		private CustomerDetails ConvertToCustomerDetailsDomainType(Models.Customer customer, IEnumerable<Models.FeatureFlag> featureFlagCustomers)
		{
			var result = default(CustomerDetails);

			if (customer != null)
			{
				result = new CustomerDetails()
				{
					ID = customer.ID
					, RecStatus = customer.RecStatus
					, RecCreatedBy = customer.RecCreatedBy
					, RecCreatedWhen = customer.RecCreatedWhen
					, RecModifiedBy = customer.RecModifiedBy
					, RecModifiedWhen = customer.RecModifiedWhen
					, RecDeletedBy = customer.RecDeletedBy
					, RecDeletedWhen = customer.RecDeletedWhen
				};

				if (featureFlagCustomers != null && featureFlagCustomers.Any())
				{
					var featureFlags = new List<FeatureFlag>();

					foreach (var item in featureFlagCustomers)
					{
						featureFlags.Add(new FeatureFlag()
						{
							ID = item.ID
							, Name = item.Name
						});
					}

					result.EnableFeatureFlags = featureFlags;
				}
			}

			return result;
		}

		private void AssignSoftDeletedPropertiesToCustomerEntityType(ref Models.Customer entityToBeDelete, Customer deleteCustomerDomain)
		{
			if (entityToBeDelete != null && deleteCustomerDomain != null)
			{
				entityToBeDelete.RecStatus = (int)RecordStatus.Delete;
				entityToBeDelete.RecDeletedBy = deleteCustomerDomain.RecDeletedBy;
				entityToBeDelete.RecDeletedWhen = DateTime.Now;
			}
		}

		#endregion
	}
}
