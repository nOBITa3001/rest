﻿namespace Bravo.FeatureFlags.Repository.EF.Repository
{
	using Bravo.FeatureFlags.Domain.Customer;
	using Bravo.FeatureFlags.Domain.FeatureFlag;
	using Bravo.Infrastructure.Common.Domain;
	using Bravo.Infrastructure.Common.Enum;
	using Bravo.Infrastructure.Common.UnitOfWork;
	using System;
	using System.Collections.Generic;
	using System.Data.Entity;
	using System.Data.Entity.Core;
	using System.Linq;

	public class FeatureFlagRepository : Repository<FeatureFlag, Guid>, IFeatureFlagRepository
	{
		#region Constructors

		public FeatureFlagRepository(IUnitOfWork unitOfWork, IObjectContextFactory objectContextFactory)
			: base(unitOfWork, objectContextFactory) { }

		#endregion

		#region Implementations

		public override FeatureFlag Get(Guid id)
		{
			var result = default(FeatureFlag);

			var featureFlag = (from x in this.DbContext.FeatureFlags
								where x.ID == id
										&& x.RecStatus >= (int)RecordStatus.Active
								select x).SingleOrDefault();
			if (featureFlag != null)
			{
				result = ConvertToDomainType(featureFlag);
			}

			return result;
		}

		public IEnumerable<FeatureFlag> GetAll()
		{
			var result = default(IEnumerable<FeatureFlag>);

			var featureFlag = (from x in this.DbContext.FeatureFlags
								where x.RecStatus >= (int)RecordStatus.Active
								select x);
			if (featureFlag != null && featureFlag.Any())
			{
				result = ConvertToDomainTypes(featureFlag);
			}

			return result;
		}

		public FeatureFlagDetails GetDetails(Guid id)
		{
			var result = default(FeatureFlagDetails);

			var featureFlag = (from x in this.DbContext.FeatureFlags
								.Include(y => y.EnableCustomers.Select(z => z.Customer))
								where x.ID == id
										&& x.RecStatus >= (int)RecordStatus.Active
								select x).SingleOrDefault();
			if (featureFlag != null)
			{
				// TODO: Try to include this condition into query above
				if (featureFlag.EnableCustomers != null && featureFlag.EnableCustomers.Count > 0)
				{
					featureFlag.EnableCustomers = featureFlag.EnableCustomers.Where(x => x.Customer.RecStatus >= (int)RecordStatus.Active).ToList();
				}
				// End TODO: Try to include this condition into query above
				result = ConvertToFeatureFlagDetailsDomainType(featureFlag, featureFlag.EnableCustomers);
			}

			return result;
		}

		[Obsolete]
		public IEnumerable<FeatureFlag> FindEnableFeatureFlags(string customerId)
		{
			throw new NotImplementedException();
		}

		[Obsolete]
		public IEnumerable<FeatureFlag> FindEnableFeatureFlags(string customerId, string appVersion)
		{
			throw new NotImplementedException();
		}

		public bool IsAuthorizedCustomer(Guid id, string customerId)
		{
			var result = false;

			var featureFlagCustomers = this.GetFeatureFlagCustomers(id);
			if (this.FeatureFlagCustomersHasEnableCustomers(featureFlagCustomers))
			{
				result = featureFlagCustomers.EnableCustomers.Any(x => x.ID.Equals(customerId, StringComparison.InvariantCultureIgnoreCase));
			}

			return result;
		}

		[Obsolete]
		public bool IsAuthorizedCustomer(string customerId, string appVersion, string featureFlagName)
		{
			throw new NotImplementedException();
		}

		public FeatureFlagCustomers GetFeatureFlagCustomers(Guid id)
		{
			var result = default(FeatureFlagCustomers);

			var featureFlag = (from x in this.DbContext.FeatureFlags
								.Include(y => y.EnableCustomers.Select(z => z.Customer))
								where x.ID == id
										&& x.RecStatus >= (int)RecordStatus.Active
								select x).SingleOrDefault();
			if (featureFlag != null)
			{
				// TODO: Try to include this condition into query above
				if (featureFlag.EnableCustomers != null && featureFlag.EnableCustomers.Count > 0)
				{
					featureFlag.EnableCustomers = featureFlag.EnableCustomers.Where(x => x.Customer.RecStatus >= (int)RecordStatus.Active).ToList();
				}
				// End TODO: Try to include this condition into query above
				result = ConvertToFeatureFlagCustomersDomainType(featureFlag, featureFlag.EnableCustomers);
			}
			else
			{
				throw new ObjectNotFoundException(string.Concat(id, " does not exist."));
			}

			return result;
		}

		public void UpdateFeatureFlagCustomers(FeatureFlagCustomers featureFlagCustomers)
		{
			this.UnitOfWork.RegisterUpdate(featureFlagCustomers, this);
		}

		public override void PersistInsert(IAggregateRoot aggregateRoot)
		{
			if (aggregateRoot is FeatureFlag)
			{
				var newFeatureFlag = (FeatureFlag)aggregateRoot;

				this.ThrowExceptionIfNewFeatureFlagIsInvalid(newFeatureFlag);

				this.DbContext.FeatureFlags.Add(ConvertToEntityType(newFeatureFlag));
			}

			this.DbContext.SaveChanges();
		}

		public override void PersistUpdate(IAggregateRoot aggregateRoot)
		{
			if (aggregateRoot is FeatureFlag)
			{
				var updateFeatureFlagDomain = (FeatureFlag)aggregateRoot;

				var existingFeatureFlagEntity = (from x in this.DbContext.FeatureFlags
													where x.ID == updateFeatureFlagDomain.ID
															&& x.RecStatus >= (int)RecordStatus.Active
													select x).SingleOrDefault();
				if (existingFeatureFlagEntity != null)
				{
					this.AssignUpdatePropertiesToFeatureFlagEntityType(ref existingFeatureFlagEntity, updateFeatureFlagDomain);

					this.ThrowExceptionIfUpdateFeatureFlagIsInvalid(existingFeatureFlagEntity);
				}
				else
				{
					throw new ObjectNotFoundException(string.Concat(updateFeatureFlagDomain.ID, " does not exist."));
				}
			}
			else if (aggregateRoot is FeatureFlagCustomers)
			{
				// TODO: Refactor
				var newFeatureFlagCustomers = (FeatureFlagCustomers)aggregateRoot;

				// Removes existing
				var existingFeatureFlagCustomers = (from x in this.DbContext.FeatureFlagCustomers
													where x.FeatureFlagID == newFeatureFlagCustomers.ID
													select x).ToList();

				if (existingFeatureFlagCustomers != null && existingFeatureFlagCustomers.Count > 0)
				{
					this.DbContext.FeatureFlagCustomers.RemoveRange(existingFeatureFlagCustomers);
				}

				// Adds new
				foreach (var item in newFeatureFlagCustomers.EnableCustomers)
				{
					this.DbContext.FeatureFlagCustomers.Add(new Models.FeatureFlagCustomer
					{
						FeatureFlagID = newFeatureFlagCustomers.ID
						, CustomerID = item.ID
					});
				}
				// End TODO: Refactor
			}

			this.DbContext.SaveChanges();
		}

		public override void PersistDelete(IAggregateRoot aggregateRoot)
		{
			if (aggregateRoot is FeatureFlag)
			{
				var deletingFeatureFlagDomain = (FeatureFlag)aggregateRoot;

				var existingFeatureFlagEntity = (from x in this.DbContext.FeatureFlags
													where x.ID == deletingFeatureFlagDomain.ID
															//TODO: Uncomment when test is done
															//&& x.RecStatus >= (int)RecordStatus.Active
													select x).SingleOrDefault();
				if (existingFeatureFlagEntity != null)
				{
					this.DbContext.FeatureFlags.Remove(existingFeatureFlagEntity);
					//TODO: Uncomment when test is done
					//this.AssignSoftDeletedPropertiesToFeatureFlagEntityType(ref existingFeatureFlagEntity, deletingFeatureFlagDomain);
				}
				else
				{
					throw new ObjectNotFoundException(string.Concat(deletingFeatureFlagDomain.ID, " does not exist."));
				}
			}

			this.DbContext.SaveChanges();
		}

		#endregion

		#region Methods

		public static FeatureFlag ConvertToDomainType(Models.FeatureFlag featureFlagEntity)
		{
			var result = default(FeatureFlag);

			if (featureFlagEntity != null)
			{
				result = new FeatureFlag
				{
					ID = featureFlagEntity.ID
					, Name = featureFlagEntity.Name
					, Description = featureFlagEntity.Description
					, RecStatus = featureFlagEntity.RecStatus
					, RecCreatedBy = featureFlagEntity.RecCreatedBy
					, RecCreatedWhen = featureFlagEntity.RecCreatedWhen
					, RecModifiedBy = featureFlagEntity.RecModifiedBy
					, RecModifiedWhen = featureFlagEntity.RecModifiedWhen
					, RecDeletedBy = featureFlagEntity.RecDeletedBy
					, RecDeletedWhen = featureFlagEntity.RecDeletedWhen
				};
			}

			return result;
		}

		public static IEnumerable<FeatureFlag> ConvertToDomainTypes(IEnumerable<Models.FeatureFlag> featureFlagEntities)
		{
			var result = default(IEnumerable<FeatureFlag>);

			if (featureFlagEntities != null && featureFlagEntities.Any())
			{
				var featureFlags = new List<FeatureFlag>();
				foreach (var featureFlagEntity in featureFlagEntities)
				{
					featureFlags.Add(ConvertToDomainType(featureFlagEntity));
				}
				result = featureFlags;
			}

			return result;
		}

		public static FeatureFlagCustomers ConvertToFeatureFlagCustomersDomainType(Models.FeatureFlag featureFlag, IEnumerable<Models.FeatureFlagCustomer> featureFlagCustomerEntities)
		{
			var result = default(FeatureFlagCustomers);

			if (featureFlag != null)
			{
				result = new FeatureFlagCustomers
				{
					ID = featureFlag.ID
					, FeatureFlag = ConvertToDomainType(featureFlag)
				};

				if (featureFlagCustomerEntities != null && featureFlagCustomerEntities.Any())
				{
					var customerEntities = featureFlagCustomerEntities.Select(x => x.Customer);
					result.EnableCustomers = CustomerRepository.ConvertToDomainTypes(customerEntities);
				}
			}

			return result;
		}

		public static FeatureFlagDetails ConvertToFeatureFlagDetailsDomainType(Models.FeatureFlag featureFlag
																				, IEnumerable<Models.FeatureFlagCustomer> featureFlagCustomers)
		{
			var result = default(FeatureFlagDetails);

			if (featureFlag != null)
			{
				result = new FeatureFlagDetails()
				{
					ID = featureFlag.ID
					, Name = featureFlag.Name
					, Description = featureFlag.Description
					, RecStatus = featureFlag.RecStatus
					, RecCreatedBy = featureFlag.RecCreatedBy
					, RecCreatedWhen = featureFlag.RecCreatedWhen
					, RecModifiedBy = featureFlag.RecModifiedBy
					, RecModifiedWhen = featureFlag.RecModifiedWhen
					, RecDeletedBy = featureFlag.RecDeletedBy
					, RecDeletedWhen = featureFlag.RecDeletedWhen
				};

				if (featureFlagCustomers != null && featureFlagCustomers.Any())
				{
					var customers = new List<Customer>();

					foreach (var item in featureFlagCustomers)
					{
						customers.Add(new Customer() { ID = item.CustomerID });
					}

					result.EnableCustomers = customers;
				}
			}

			return result;
		}

		public static Models.FeatureFlag ConvertToEntityType(FeatureFlag featureFlagDomain)
		{
			var result = default(Models.FeatureFlag);

			if (featureFlagDomain != null)
			{
				result = new Models.FeatureFlag
				{
					ID = featureFlagDomain.ID
					, Name = featureFlagDomain.Name
					, Description = featureFlagDomain.Description
					, RecStatus = featureFlagDomain.RecStatus
					, RecCreatedBy = featureFlagDomain.RecCreatedBy
					, RecCreatedWhen = featureFlagDomain.RecCreatedWhen
					, RecModifiedBy = featureFlagDomain.RecModifiedBy
					, RecModifiedWhen = featureFlagDomain.RecModifiedWhen
					, RecDeletedBy = featureFlagDomain.RecDeletedBy
					, RecDeletedWhen = featureFlagDomain.RecDeletedWhen
				};
			}

			return result;
		}

		private bool FeatureFlagCustomersHasEnableCustomers(FeatureFlagCustomers featureFlagCustomers)
		{
			return (featureFlagCustomers != null
					&& featureFlagCustomers.EnableCustomers != null
					&& featureFlagCustomers.EnableCustomers.Any());
		}

		private void ThrowExceptionIfNewFeatureFlagIsInvalid(FeatureFlag newFeatureFlag)
		{
			var existingFeatureFlag = (from x in this.DbContext.FeatureFlags
										where x.ID == newFeatureFlag.ID
												|| x.Name.Equals(newFeatureFlag.Name, StringComparison.InvariantCultureIgnoreCase)
										select x).ToList();
			if (existingFeatureFlag.Count > 0)
			{
				throw new ArgumentException(this.GenerateInsertFeatureFlagErrorMessage(newFeatureFlag, existingFeatureFlag));
			}
		}

		private string GenerateInsertFeatureFlagErrorMessage(FeatureFlag newFeatureFlag, List<Models.FeatureFlag> existingFeatureFlag)
		{
			var result = string.Empty;

			var doesIDExist = existingFeatureFlag.Any(x => x.ID == newFeatureFlag.ID);
			var doesNameExist = existingFeatureFlag.Any(x => x.Name.Equals(newFeatureFlag.Name, StringComparison.InvariantCultureIgnoreCase));
			if (doesIDExist && doesNameExist)
			{
				result = string.Format("{0} and {1} already exist.", newFeatureFlag.ID, newFeatureFlag.Name);
			}
			else if (doesIDExist)
			{
				result = string.Format("{0} already exists.", newFeatureFlag.ID);
			}
			else if (doesNameExist)
			{
				result = string.Format("{0} already exists.", newFeatureFlag.Name);
			}

			return result;
		}

		private void ThrowExceptionIfUpdateFeatureFlagIsInvalid(Models.FeatureFlag updateFeatureFlag)
		{
			var existingFeatureFlag = (from x in this.DbContext.FeatureFlags
										where x.ID != updateFeatureFlag.ID
												&& x.Name.Equals(updateFeatureFlag.Name, StringComparison.InvariantCultureIgnoreCase)
										select x).ToList();
			if (existingFeatureFlag.Count > 0)
			{
				throw new ArgumentException(string.Format("{0} already exists.", updateFeatureFlag.Name));
			}
		}

		private void AssignUpdatePropertiesToFeatureFlagEntityType(ref Models.FeatureFlag entityToBeUpdate, FeatureFlag updateFeatureFlagDomain)
		{
			if (entityToBeUpdate != null && updateFeatureFlagDomain != null)
			{
				entityToBeUpdate.Name = updateFeatureFlagDomain.Name;
				entityToBeUpdate.Description = updateFeatureFlagDomain.Description;
				entityToBeUpdate.RecModifiedBy = updateFeatureFlagDomain.RecModifiedBy;
				entityToBeUpdate.RecModifiedWhen = updateFeatureFlagDomain.RecModifiedWhen;
			}
		}

		private void AssignSoftDeletedPropertiesToFeatureFlagEntityType(ref Models.FeatureFlag entityToBeDelete, FeatureFlag deleteFeatureFlagDomain)
		{
			if (entityToBeDelete != null && deleteFeatureFlagDomain != null)
			{
				entityToBeDelete.RecStatus = (int)RecordStatus.Delete;
				entityToBeDelete.RecDeletedBy = deleteFeatureFlagDomain.RecDeletedBy;
				entityToBeDelete.RecDeletedWhen = DateTime.Now;
			}
		}

		#endregion
	}
}
