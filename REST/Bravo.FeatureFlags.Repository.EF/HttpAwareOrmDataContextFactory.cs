﻿namespace Bravo.FeatureFlags.Repository.EF
{
	using Models;
	using System.Web;

	public class HttpAwareOrmDataContextFactory : IObjectContextFactory
	{
		private const string DATA_CONTEXT_KEY = "DataContext";

		public FeatureFlagContext Create()
		{
			var objectContext = default(FeatureFlagContext);

			if (HttpContext.Current.Items.Contains(DATA_CONTEXT_KEY))
			{
				objectContext = HttpContext.Current.Items[DATA_CONTEXT_KEY] as FeatureFlagContext;
			}
			else
			{
				objectContext = new FeatureFlagContext();
				this.Store(objectContext);
			}

			return objectContext;
		}

		private void Store(FeatureFlagContext objectContext)
		{
			if (HttpContext.Current.Items.Contains(DATA_CONTEXT_KEY))
			{
				HttpContext.Current.Items[DATA_CONTEXT_KEY] = objectContext;
			}
			else
			{
				HttpContext.Current.Items.Add(DATA_CONTEXT_KEY, objectContext);
			}
		}
	}
}
