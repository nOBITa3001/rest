namespace Bravo.FeatureFlags.Repository.EF.Migrations_FeatureFlagContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitializeFeatureFlag : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Customer",
                c => new
                    {
                        ID = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.FeatureFlagCustomer",
                c => new
                    {
                        FeatureFlagID = c.Guid(nullable: false),
                        CustomerID = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.FeatureFlagID, t.CustomerID })
                .ForeignKey("dbo.Customer", t => t.CustomerID, cascadeDelete: true)
                .ForeignKey("dbo.FeatureFlag", t => t.FeatureFlagID, cascadeDelete: true)
                .Index(t => t.FeatureFlagID)
                .Index(t => t.CustomerID);
            
            CreateTable(
                "dbo.FeatureFlag",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FeatureFlagCustomer", "FeatureFlagID", "dbo.FeatureFlag");
            DropForeignKey("dbo.FeatureFlagCustomer", "CustomerID", "dbo.Customer");
            DropIndex("dbo.FeatureFlagCustomer", new[] { "CustomerID" });
            DropIndex("dbo.FeatureFlagCustomer", new[] { "FeatureFlagID" });
            DropTable("dbo.FeatureFlag");
            DropTable("dbo.FeatureFlagCustomer");
            DropTable("dbo.Customer");
        }
    }
}
