namespace Bravo.FeatureFlags.Repository.EF.Migrations_FeatureFlagContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SetUniqueConstraintFeatureFlagName : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.FeatureFlag", "Name", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("dbo.FeatureFlag", new[] { "Name" });
        }
    }
}
