namespace Bravo.FeatureFlags.Repository.EF.Migrations_FeatureFlagContext
{
	using Bravo.FeatureFlags.Repository.EF.Models;
	using System;
	using System.Collections.Generic;
	using System.Data.Entity.Migrations;

	public sealed class Configuration : DbMigrationsConfiguration<FeatureFlagContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            MigrationsDirectory = @"Migrations_FeatureFlagContext";
        }

		/// <summary>
		/// Manipulates data when DbMigrationsConfiguration.
		/// </summary>
        protected override void Seed(FeatureFlagContext context)
        {
#if DEBUG
			context.FeatureFlags.AddOrUpdate(x => x.ID
				, new FeatureFlag { ID = new Guid("3dd53662-b4d5-4bd8-83f8-7992d33f8d37"), Name = "WeightAndBalance", RecStatus = 1, RecCreatedBy = "InitialUser", RecCreatedWhen = DateTime.Now }
				, new FeatureFlag { ID = new Guid("e6fd1083-721a-4262-8629-149751d44821"), Name = "Printing", RecStatus = 1, RecCreatedBy = "InitialUser", RecCreatedWhen = DateTime.Now }
				, new FeatureFlag { ID = new Guid("6bbb3b0e-98ae-4fb9-9e17-6554896e379a"), Name = "Read", RecStatus = 1, RecCreatedBy = "InitialUser", RecCreatedWhen = DateTime.Now }
				, new FeatureFlag { ID = new Guid("fd91fcc6-3207-41ca-ad37-661c487d24da"), Name = "Write", RecStatus = 1, RecCreatedBy = "InitialUser", RecCreatedWhen = DateTime.Now }
				, new FeatureFlag { ID = new Guid("d5ac881c-f236-487f-91d4-d6f90b446486"), Name = "Modify", RecStatus = 1, RecCreatedBy = "InitialUser", RecCreatedWhen = DateTime.Now }
				, new FeatureFlag { ID = new Guid("2af28a55-08b9-4629-beae-9f26dcc81280"), Name = "Delete", RecStatus = 1, RecCreatedBy = "InitialUser", RecCreatedWhen = DateTime.Now }
			);

			context.Customers.AddOrUpdate(x => x.ID
				, new Customer { ID = "PEACH", RecStatus = 1, RecCreatedBy = "InitialUser", RecCreatedWhen = DateTime.Now }
				, new Customer { ID = "EDW", RecStatus = 1, RecCreatedBy = "InitialUser", RecCreatedWhen = DateTime.Now }
				, new Customer { ID = "QANTAS", RecStatus = 1, RecCreatedBy = "InitialUser", RecCreatedWhen = DateTime.Now }
				, new Customer { ID = "JET", RecStatus = 1, RecCreatedBy = "InitialUser", RecCreatedWhen = DateTime.Now }
				, new Customer { ID = "TG", RecStatus = 1, RecCreatedBy = "InitialUser", RecCreatedWhen = DateTime.Now }
			);

			context.FeatureFlagCustomers.AddOrUpdate(x => new { x.FeatureFlagID, x.CustomerID }
				// WeightAndBalance feature flag.
				, new FeatureFlagCustomer { FeatureFlagID = new Guid("3dd53662-b4d5-4bd8-83f8-7992d33f8d37"), CustomerID = "PEACH" }

				// Printing feature flag.
				, new FeatureFlagCustomer { FeatureFlagID = new Guid("e6fd1083-721a-4262-8629-149751d44821"), CustomerID = "PEACH" }
				, new FeatureFlagCustomer { FeatureFlagID = new Guid("e6fd1083-721a-4262-8629-149751d44821"), CustomerID = "EDW" }
				, new FeatureFlagCustomer { FeatureFlagID = new Guid("e6fd1083-721a-4262-8629-149751d44821"), CustomerID = "QANTAS" }

				// Read feature flag.
				, new FeatureFlagCustomer { FeatureFlagID = new Guid("6bbb3b0e-98ae-4fb9-9e17-6554896e379a"), CustomerID = "PEACH" }
				, new FeatureFlagCustomer { FeatureFlagID = new Guid("6bbb3b0e-98ae-4fb9-9e17-6554896e379a"), CustomerID = "EDW" }
				, new FeatureFlagCustomer { FeatureFlagID = new Guid("6bbb3b0e-98ae-4fb9-9e17-6554896e379a"), CustomerID = "QANTAS" }
				, new FeatureFlagCustomer { FeatureFlagID = new Guid("6bbb3b0e-98ae-4fb9-9e17-6554896e379a"), CustomerID = "JET" }
				, new FeatureFlagCustomer { FeatureFlagID = new Guid("6bbb3b0e-98ae-4fb9-9e17-6554896e379a"), CustomerID = "TG" }

				// Write feature flag.
				, new FeatureFlagCustomer { FeatureFlagID = new Guid("fd91fcc6-3207-41ca-ad37-661c487d24da"), CustomerID = "PEACH" }
				, new FeatureFlagCustomer { FeatureFlagID = new Guid("fd91fcc6-3207-41ca-ad37-661c487d24da"), CustomerID = "EDW" }
				, new FeatureFlagCustomer { FeatureFlagID = new Guid("fd91fcc6-3207-41ca-ad37-661c487d24da"), CustomerID = "QANTAS" }
				, new FeatureFlagCustomer { FeatureFlagID = new Guid("fd91fcc6-3207-41ca-ad37-661c487d24da"), CustomerID = "JET" }
				, new FeatureFlagCustomer { FeatureFlagID = new Guid("fd91fcc6-3207-41ca-ad37-661c487d24da"), CustomerID = "TG" }

				// Modify feature flag.
				, new FeatureFlagCustomer { FeatureFlagID = new Guid("d5ac881c-f236-487f-91d4-d6f90b446486"), CustomerID = "PEACH" }
				, new FeatureFlagCustomer { FeatureFlagID = new Guid("d5ac881c-f236-487f-91d4-d6f90b446486"), CustomerID = "EDW" }
				, new FeatureFlagCustomer { FeatureFlagID = new Guid("d5ac881c-f236-487f-91d4-d6f90b446486"), CustomerID = "QANTAS" }
				, new FeatureFlagCustomer { FeatureFlagID = new Guid("d5ac881c-f236-487f-91d4-d6f90b446486"), CustomerID = "JET" }
				, new FeatureFlagCustomer { FeatureFlagID = new Guid("d5ac881c-f236-487f-91d4-d6f90b446486"), CustomerID = "TG" }

				// Delete feature flag.
				, new FeatureFlagCustomer { FeatureFlagID = new Guid("2af28a55-08b9-4629-beae-9f26dcc81280"), CustomerID = "PEACH" }
				, new FeatureFlagCustomer { FeatureFlagID = new Guid("2af28a55-08b9-4629-beae-9f26dcc81280"), CustomerID = "EDW" }
				, new FeatureFlagCustomer { FeatureFlagID = new Guid("2af28a55-08b9-4629-beae-9f26dcc81280"), CustomerID = "QANTAS" }
				, new FeatureFlagCustomer { FeatureFlagID = new Guid("2af28a55-08b9-4629-beae-9f26dcc81280"), CustomerID = "JET" }
				, new FeatureFlagCustomer { FeatureFlagID = new Guid("2af28a55-08b9-4629-beae-9f26dcc81280"), CustomerID = "TG" }
			);

			context.SaveChanges();
#endif
		}
    }
}
