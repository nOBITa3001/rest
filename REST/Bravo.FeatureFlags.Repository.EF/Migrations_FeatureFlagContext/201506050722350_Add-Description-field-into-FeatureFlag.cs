namespace Bravo.FeatureFlags.Repository.EF.Migrations_FeatureFlagContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDescriptionfieldintoFeatureFlag : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FeatureFlag", "Description", c => c.String(maxLength: 500));
        }
        
        public override void Down()
        {
            DropColumn("dbo.FeatureFlag", "Description");
        }
    }
}
