namespace Bravo.FeatureFlags.Repository.EF.Migrations_FeatureFlagContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRecordHistoryintoFeatureFlagandCustomer : DbMigration
    {
        public override void Up()
        {
			var defaultDateTime = DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd"), "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);

            AddColumn("dbo.Customer", "RecStatus", c => c.Int(nullable: false, defaultValue: 1));
            AddColumn("dbo.Customer", "RecCreatedBy", c => c.String(nullable: false, maxLength: 256, defaultValue: "InitialUser"));
			AddColumn("dbo.Customer", "RecCreatedWhen", c => c.DateTime(nullable: false, defaultValue: defaultDateTime));
            AddColumn("dbo.Customer", "RecModifiedBy", c => c.String(maxLength: 256));
            AddColumn("dbo.Customer", "RecModifiedWhen", c => c.DateTime());
            AddColumn("dbo.Customer", "RecDeletedBy", c => c.String(maxLength: 256));
            AddColumn("dbo.Customer", "RecDeletedWhen", c => c.DateTime());
			AddColumn("dbo.FeatureFlag", "RecStatus", c => c.Int(nullable: false, defaultValue: 1));
			AddColumn("dbo.FeatureFlag", "RecCreatedBy", c => c.String(nullable: false, maxLength: 256, defaultValue: "InitialUser"));
			AddColumn("dbo.FeatureFlag", "RecCreatedWhen", c => c.DateTime(nullable: false, defaultValue: defaultDateTime));
            AddColumn("dbo.FeatureFlag", "RecModifiedBy", c => c.String(maxLength: 256));
            AddColumn("dbo.FeatureFlag", "RecModifiedWhen", c => c.DateTime());
            AddColumn("dbo.FeatureFlag", "RecDeletedBy", c => c.String(maxLength: 256));
            AddColumn("dbo.FeatureFlag", "RecDeletedWhen", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.FeatureFlag", "RecDeletedWhen");
            DropColumn("dbo.FeatureFlag", "RecDeletedBy");
            DropColumn("dbo.FeatureFlag", "RecModifiedWhen");
            DropColumn("dbo.FeatureFlag", "RecModifiedBy");
            DropColumn("dbo.FeatureFlag", "RecCreatedWhen");
            DropColumn("dbo.FeatureFlag", "RecCreatedBy");
            DropColumn("dbo.FeatureFlag", "RecStatus");
            DropColumn("dbo.Customer", "RecDeletedWhen");
            DropColumn("dbo.Customer", "RecDeletedBy");
            DropColumn("dbo.Customer", "RecModifiedWhen");
            DropColumn("dbo.Customer", "RecModifiedBy");
            DropColumn("dbo.Customer", "RecCreatedWhen");
            DropColumn("dbo.Customer", "RecCreatedBy");
            DropColumn("dbo.Customer", "RecStatus");
        }
    }
}
