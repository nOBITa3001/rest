﻿namespace Bravo.FeatureFlags.Repository.EF
{
	using Models;

	public interface IObjectContextFactory
	{
		FeatureFlagContext Create();
	}
}
