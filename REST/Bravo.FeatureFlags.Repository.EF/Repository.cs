﻿namespace Bravo.FeatureFlags.Repository.EF
{
	using Bravo.FeatureFlags.Repository.EF.Models;
	using Bravo.Infrastructure.Common.Domain;
	using Bravo.Infrastructure.Common.UnitOfWork;
	using System;

	public abstract class Repository<DomainType, IDType> : IUnitOfWorkRepository
		where DomainType : IAggregateRoot
	{
		#region Declarations

		private readonly IUnitOfWork unitOfWork;
		private readonly FeatureFlagContext dbContext;

		#endregion

		#region Properties

		/// <summary>
		/// Gets unit of work.
		/// </summary>
		public IUnitOfWork UnitOfWork
		{
			get
			{
				return this.unitOfWork;
			}
		}

		/// <summary>
		/// Gets DB context.
		/// </summary>
		public FeatureFlagContext DbContext
		{
			get
			{
				return this.dbContext;
			}
		}

		#endregion

		#region Constructors

		public Repository(IUnitOfWork unitOfWork, IObjectContextFactory objectContextFactory)
		{
			this.unitOfWork = unitOfWork;
			this.dbContext = objectContextFactory.Create();

			this.ThrowExceptionIfRepositoryIsInvalid();
		}

		#endregion

		#region Implementations

		public abstract DomainType Get(IDType id);

		public abstract void PersistInsert(IAggregateRoot aggregateRoot);

		public abstract void PersistUpdate(IAggregateRoot aggregateRoot);

		public abstract void PersistDelete(IAggregateRoot aggregateRoot);

		#endregion

		#region Methods

		private void ThrowExceptionIfRepositoryIsInvalid()
		{
			if (this.unitOfWork == null)
			{
				throw new ArgumentNullException("UnitOfWork in Repository");
			}

			if (this.dbContext == null)
			{
				throw new ArgumentNullException("DBContext in Repository");
			}
		}

		public void Insert(DomainType aggregate)
		{
			this.unitOfWork.RegisterInsert(aggregate, this);
		}

		public void Update(DomainType aggregate)
		{
			this.unitOfWork.RegisterUpdate(aggregate, this);
		}

		public void Delete(DomainType aggregate)
		{
			this.unitOfWork.RegisterDelete(aggregate, this);
		}

		#endregion
	}
}
