﻿[assembly: Microsoft.Owin.OwinStartup(typeof(REST.WebAPI.Startup))]

namespace REST.WebAPI
{
	using Microsoft.Owin;
	using Microsoft.Owin.Cors;
	using Microsoft.Owin.Security;
	using Microsoft.Owin.Security.DataHandler.Encoder;
	using Microsoft.Owin.Security.Jwt;
	using Microsoft.Owin.Security.OAuth;
	using Owin;
	using REST.WebAPI.Formats;
	using REST.WebAPI.Providers;
	using System;

	public class Startup
	{
		public void Configuration(IAppBuilder app)
		{
			//var config = new HttpConfiguration();

			//// Web API routes
			//config.MapHttpAttributeRoutes();

			this.ConfigureOAuth(app);

			app.UseCors(CorsOptions.AllowAll);

			//app.UseWebApi(config);
		}

		public void ConfigureOAuth(IAppBuilder app)
		{
			var OAuthServerOptions = new OAuthAuthorizationServerOptions()
			{
				//For Dev enviroment only (on production should be AllowInsecureHttp = false)
				AllowInsecureHttp = true,
				TokenEndpointPath = new PathString("/oauth2/token"),
				AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(30),
				Provider = new CustomOAuthProvider(),
				AccessTokenFormat = new CustomJwtFormat("http://www.restworkshop.net")
			};

			// OAuth 2.0 Bearer Access Token Generation
			app.UseOAuthAuthorizationServer(OAuthServerOptions);

			var issuer = "http://www.restworkshop.net";
			var audience = "099153c2625149bc8ecb3e85e03f0022";
			var secret = TextEncodings.Base64Url.Decode("IxrAjDoa2FqElO7IhrSrUJELhUckePEPVpaePlS_Xaw");

			// Api controllers with an [Authorize] attribute will be validated with JWT
			app.UseJwtBearerAuthentication(new JwtBearerAuthenticationOptions
			{
				AuthenticationMode = AuthenticationMode.Active
				, AllowedAudiences = new[] { audience }
				, IssuerSecurityTokenProviders = new IIssuerSecurityTokenProvider[]
				{
					new SymmetricKeyIssuerSecurityTokenProvider(issuer, secret)
				}
			});
		}
	}
}
