﻿namespace REST.WebAPI.ViewModels
{
	using System;
	using System.ComponentModel.DataAnnotations;

	public class FeatureFlagViewModel
	{
		public Guid? ID { get; set; }

		[Required]
		[StringLength(100)]
		public string Name { get; set; }

		[StringLength(500)]
		public string Description { get; set; }
	}
}