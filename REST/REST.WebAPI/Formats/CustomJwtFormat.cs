﻿namespace REST.WebAPI.Formats
{
	using Microsoft.Owin.Security;
	using Microsoft.Owin.Security.DataHandler.Encoder;
	using System;
	using System.IdentityModel.Tokens;
	using Thinktecture.IdentityModel.Tokens;

	public class CustomJwtFormat : ISecureDataFormat<AuthenticationTicket>
	{
		#region Declarations

		private const string AUDIENCE_PROPERTY_KEY = "audience";
		private readonly string issuer = string.Empty;

		#endregion

		#region Constructures

		public CustomJwtFormat(string issuer)
		{
			this.issuer = issuer;
		}

		#endregion

		#region Implementations

		public string Protect(AuthenticationTicket data)
		{
			if (data == null)
			{
				throw new ArgumentNullException("data");
			}

			var audienceID = data.Properties.Dictionary.ContainsKey(AUDIENCE_PROPERTY_KEY)
								? data.Properties.Dictionary[AUDIENCE_PROPERTY_KEY]
								: null;

			if (string.IsNullOrWhiteSpace(audienceID))
			{
				throw new InvalidOperationException("AuthenticationTicket.Properties does not include audience");
			}

			var audience = AudiencesStore.FindAudience(audienceID);

			var symmetricKeyAsBase64 = audience.Base64Secret;
  
			var keyByteArray = TextEncodings.Base64Url.Decode(symmetricKeyAsBase64);

			var signingKey = new HmacSigningCredentials(keyByteArray);

			var issued = data.Properties.IssuedUtc;
			var expires = data.Properties.ExpiresUtc;

			var token = new JwtSecurityToken(this.issuer, audienceID, data.Identity.Claims
												, issued.Value.UtcDateTime, expires.Value.UtcDateTime, signingKey);

			var handler = new JwtSecurityTokenHandler();

			var jwt = handler.WriteToken(token);

			return jwt;
		}

		public AuthenticationTicket Unprotect(string protectedText)
		{
			throw new NotImplementedException();
		}

		#endregion
	}
}