﻿namespace REST.WebAPI.Conversion
{
	using Bravo.FeatureFlags.Domain.FeatureFlag;
	using ViewModels;

	public static class DomainConverter
	{
		/// <summary>
		/// Converts to feature flag view model.
		/// </summary>
		public static FeatureFlagViewModel ConvertToFeatureFlagViewModel(FeatureFlag domain)
		{
			var result = default(FeatureFlagViewModel);

			if (domain != null)
			{
				result = new FeatureFlagViewModel()
				{
					ID = domain.ID
					, Name = domain.Name
					, Description = domain.Description
				};
			}

			return result;
		}
	}
}