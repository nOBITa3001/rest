﻿namespace REST.WebAPI.Controllers
{
	using Bravo.FeatureFlags.ApplicationService.Interface;
	using Bravo.FeatureFlags.ApplicationService.Messaging.FeatureFlag;
	using REST.WebAPI.Conversion;
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Net;
	using System.Web.Http;
	using ViewModels;

	[Authorize]
	[RoutePrefix("api")]
	public class FeatureFlagController : ApiController
	{
		private readonly IFeatureFlagService featureFlagService;

		public FeatureFlagController(IFeatureFlagService featureFlagService)
		{
			this.featureFlagService = featureFlagService;

			this.ThrowExceptionIfControllerIsInvalid();
		}

		#region Actions

		/// <summary>
		/// Route ex. www.webapiserver.com/api/featureflag
		/// </summary>
		[AllowAnonymous]
		public IHttpActionResult Get(int page = 1, int pageSize = 10)
		{
			var result = default(IHttpActionResult);

			var response = this.featureFlagService.GetAll();
			if (response.Exception != null)
			{
				result = this.InternalServerError(response.Exception);
			}
			else
			{
				this.ApplyPaging(ref response, page, pageSize);

				result = this.Ok(response.FeatureFlags);
			}

			return result;
		}

		/// <summary>
		/// Route ex. www.webapiserver.com/api/v2/featureflag
		/// </summary>
		[AllowAnonymous]
		[HttpGet]
		[Route("v2/featureflag")]
		public IHttpActionResult Get_V2(int page = 1, int pageSize = 10)
		{
			var result = default(IHttpActionResult);

			var response = this.featureFlagService.GetAll();

			if (response.Exception != null)
			{
				result = this.InternalServerError(response.Exception);
			}
			else
			{
				this.ApplyPaging(ref response, page, pageSize);

				var viewModel = new Dictionary<Bravo.FeatureFlags.ApplicationService.ViewModels.FeatureFlagViewModel, string>();
				var rd = new Random();
				foreach (var item in response.FeatureFlags)
				{
					viewModel.Add(item, string.Concat("Rank: ", rd.Next(1, 5)));
				}

				result = this.Ok(new
				{
					Version = "2.0.0"
					, FeatureFlags = viewModel.ToArray()
				});
			}

			return result;
		}

		/// <summary>
		/// Route ex. www.webapiserver.com/api/featureflag/details/{id}
		/// </summary>
		[Route("featureflag/details/{id}")]
		[HttpGet]
		[AllowAnonymous]
		public IHttpActionResult Details(Guid id)
		{
			var result = default(IHttpActionResult);

			if (id == Guid.Empty)
			{
				result = this.BadRequest("Please provide an ID.");
			}
			else
			{
				var getFeatureFlagRequest = new GetFeatureFlagRequest(id);

				var response = this.featureFlagService.Get(getFeatureFlagRequest);
				if (response.Exception != null)
				{
					result = this.InternalServerError(response.Exception);
				}
				else if (response.FeatureFlag == null)
				{
					result = this.NotFound();
				}
				else
				{
					var viewModel = DomainConverter.ConvertToFeatureFlagViewModel(response.FeatureFlag);
					result = this.Ok(viewModel);
				}
			}

			return result;
		}

		/// <summary>
		/// Route ex. www.webapiserver.com/api/featureflag
		/// </summary>
		public IHttpActionResult Post(FeatureFlagViewModel featureFlag)
		{
			var result = default(IHttpActionResult);

			if (this.ModelState.IsValid)
			{
				var insertFeatureFlagRequest = this.CreateInsertFeatureFlagRequest(featureFlag);

				var response = this.featureFlagService.Insert(insertFeatureFlagRequest);
				if (response.Exception != null)
				{
					// Can have a logic for determine the response's exception and return a proper response.
					// For instance, the exception is a creating feature flag that already exist.
					// So, the proper response should be 409 Conflict, not 500.
					result = this.InternalServerError(response.Exception);
				}
				else
				{
					featureFlag.ID = insertFeatureFlagRequest.ID;
					var location = new Uri(string.Concat("http://localhost:53482/api/featureflag/details/", featureFlag.ID));
					result = this.Created(location, featureFlag);
				}
			}
			else
			{
				result = this.BadRequest();
			}

			return result;
		}

		/// <summary>
		/// Route ex. www.webapiserver.com/api/featureflag
		/// </summary>
		public IHttpActionResult Put(FeatureFlagViewModel featureFlag)
		{
			var result = default(IHttpActionResult);

			if (this.ModelState.IsValid && featureFlag.ID.HasValue)
			{
				var updateFeatureFlagRequest = this.CreateUpdateFeatureFlagRequest(featureFlag);

				var response = this.featureFlagService.Update(updateFeatureFlagRequest);
				if (response.Exception != null)
				{
					// Can have a logic for determine the response's exception and return a proper response.
					// For instance, the exception is an updating feature flag that not exist.
					// So, the proper response should be 404 Not Found, not 500.
					result = this.InternalServerError(response.Exception);
				}
				else
				{
					result = this.Ok();
				}
			}
			else
			{
				result = this.BadRequest();
			}

			return result;
		}

		/// <summary>
		/// Route ex. www.webapiserver.com/api/featureflag
		/// </summary>
		public IHttpActionResult Delete(Guid id)
		{
			var result = default(IHttpActionResult);

			if (id == Guid.Empty)
			{
				result = this.BadRequest("Please provide an ID.");
			}
			else
			{
				var deleteFeatureFlagRequest = this.CreateDeleteFeatureFlagRequest(id);

				var response = this.featureFlagService.Delete(deleteFeatureFlagRequest);
				if (response.Exception != null)
				{
					// Can have a logic for determine the response's exception and return a proper response.
					// For instance, the exception is a deleting feature flag that not exist.
					// So, the proper response should be 404 Not Found, not 500.
					result = this.InternalServerError(response.Exception);
				}
				else
				{
					// Following W3C - http://www.w3.org/Protocols/rfc2616/rfc2616.html
					result = this.StatusCode(HttpStatusCode.NoContent);
				}
			}

			return result;
		}

		#endregion

		#region Methods

		private void ThrowExceptionIfControllerIsInvalid()
		{
			if (this.featureFlagService == null)
			{
				throw new ArgumentNullException("FeatureFlagService in FeatureFlagController");
			}
		}

		/// <summary>
		/// Applies paging for get feature flags.
		/// </summary>
		private void ApplyPaging(ref GetFeatureFlagsResponse response, int page, int pageSize)
		{
			response.FeatureFlags = response.FeatureFlags.Skip(--page * pageSize).Take(pageSize);
		}

		private InsertFeatureFlagRequest CreateInsertFeatureFlagRequest(FeatureFlagViewModel featureFlag)
		{
			var result = new InsertFeatureFlagRequest(featureFlag.ID.GetValueOrDefault(Guid.NewGuid()));
			result.FeatureFlagProperties.Name = featureFlag.Name;
			result.FeatureFlagProperties.Description = featureFlag.Description;
			result.FeatureFlagProperties.UserName = "TestUser";

			return result;
		}

		private UpdateFeatureFlagRequest CreateUpdateFeatureFlagRequest(FeatureFlagViewModel featureFlagViewModel)
		{
			var result = new UpdateFeatureFlagRequest(featureFlagViewModel.ID.Value);
			result.FeatureFlagProperties.Name = featureFlagViewModel.Name;
			result.FeatureFlagProperties.Description = featureFlagViewModel.Description;
			result.FeatureFlagProperties.UserName = "TestUser";

			return result;
		}

		private DeleteFeatureFlagRequest CreateDeleteFeatureFlagRequest(Guid id)
		{
			var result = new DeleteFeatureFlagRequest(id);
			result.FeatureFlagProperties.UserName = "TestUser";
			return result;
		}

		#endregion
	}
}
