﻿namespace REST.WebAPI.Controllers
{
	using Entities;
	using Models;
	using System.Web.Http;

	[RoutePrefix("api/audience")]
	public class AudienceController : ApiController
	{
		public IHttpActionResult Post(AudienceModel audienceModel)
		{
			if (!this.ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			var newAudience = AudiencesStore.AddAudience(audienceModel.Name);

			return this.Ok<Audience>(newAudience);
		}
	}
}
