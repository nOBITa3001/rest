[assembly: WebActivator.PostApplicationStartMethod(typeof(REST.WebAPI.App_Start.SimpleInjectorWebApiInitializer), "Initialize")]

namespace REST.WebAPI.App_Start
{
	using Bravo.FeatureFlags.ApplicationService.Implementation;
	using Bravo.FeatureFlags.ApplicationService.Interface;
	using Bravo.FeatureFlags.Domain.FeatureFlag;
	using Bravo.FeatureFlags.Repository.EF;
	using Bravo.FeatureFlags.Repository.EF.Repository;
	using Bravo.Infrastructure.Common.Caching;
	using Bravo.Infrastructure.Common.UnitOfWork;
	using SimpleInjector;
	using SimpleInjector.Integration.WebApi;
	using System.Web;
	using System.Web.Http;
	
	public static class SimpleInjectorWebApiInitializer
	{
		/// <summary>Initialize the container and register it as Web API Dependency Resolver.</summary>
		public static void Initialize()
		{
			var container = new Container();
			container.Options.DefaultScopedLifestyle = new WebApiRequestLifestyle();
			
			InitializeContainer(container);

			container.RegisterWebApiControllers(GlobalConfiguration.Configuration);
	   
			container.Verify();
			
			GlobalConfiguration.Configuration.DependencyResolver =
				new SimpleInjectorWebApiDependencyResolver(container);
		}
	 
		private static void InitializeContainer(Container container)
		{
			#region Services

			container.RegisterWebApiRequest<IFeatureFlagService, FeatureFlagService>();

			#endregion

			#region Repositories

			container.RegisterWebApiRequest<IFeatureFlagRepository, FeatureFlagRepository>();

			#endregion

			#region Common

			container.RegisterWebApiRequest<IUnitOfWork>(() =>
			{
				var items = HttpContext.Current.Items;
				var uow = (IUnitOfWork)items["UnitOfWork"];
				if (uow == null)
				{
					items["UnitOfWork"] = uow = container.GetInstance<EFUnitOfWork>();
				}
				return uow;
			});
			container.RegisterWebApiRequest<IObjectContextFactory, HttpAwareOrmDataContextFactory>();
			container.RegisterWebApiRequest<ICacheStorage, SystemRuntimeCacheStorage>();

			#endregion
		}
	}
}