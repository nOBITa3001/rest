﻿namespace Bravo.FeatureFlags.ApplicationService.Interface
{
	using Bravo.FeatureFlags.ApplicationService.Messaging.Customer;

	public interface ICustomerService
	{
		GetCustomerResponse Get(GetCustomerRequest getCustomerRequest);
		GetCustomerDetailsResponse GetDetails(GetCustomerRequest getCustomerRequest);
		GetCustomersResponse GetAll();
		InsertCustomerResponse Insert(InsertCustomerRequest insertCustomerRequest);
		UpdateCustomerResponse Update(UpdateCustomerRequest updateCustomerRequest);
		UpdateCustomerResponse UpdateCustomerFeatureFlags(UpdateCustomerRequest updateCustomerRequest);
		DeleteCustomerResponse Delete(DeleteCustomerRequest deleteCustomerRequest);
		GetEnableFeatureFlagsResponse GetEnableFeatureFlags(GetEnableFeatureFlagsRequest getEnableFeatureFlagsRequest);
	}
}
