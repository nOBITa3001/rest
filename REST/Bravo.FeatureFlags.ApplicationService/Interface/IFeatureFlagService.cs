﻿namespace Bravo.FeatureFlags.ApplicationService.Interface
{
	using Bravo.FeatureFlags.ApplicationService.Messaging.FeatureFlag;

	public interface IFeatureFlagService
	{
		GetFeatureFlagResponse Get(GetFeatureFlagRequest getFeatureFlagRequest);
		GetFeatureFlagDetailsResponse GetDetails(GetFeatureFlagRequest getFeatureFlagRequest);
		GetFeatureFlagsResponse GetAll();
		InsertFeatureFlagResponse Insert(InsertFeatureFlagRequest insertFeatureFlagRequest);
		UpdateFeatureFlagResponse Update(UpdateFeatureFlagRequest updateFeatureFlagRequest);
		UpdateFeatureFlagResponse UpdateFeatureFlagCustomers(UpdateFeatureFlagRequest updateFeatureFlagRequest);
		DeleteFeatureFlagResponse Delete(DeleteFeatureFlagRequest deleteFeatureFlagRequest);
		GetFeatureFlagAuthorizationResponse GetAuthorization(GetFeatureFlagAuthorizationRequest getFeatureFlagAuthorizationRequest);
	}
}
