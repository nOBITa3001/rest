﻿namespace Bravo.FeatureFlags.ApplicationService.ModelConversion
{
	using Bravo.FeatureFlags.ApplicationService.ViewModels;
	using Bravo.FeatureFlags.Domain.Customer;
	using Bravo.FeatureFlags.Domain.FeatureFlag;
	using System.Collections.Generic;
	using System.Linq;

	public static class ConversionHelper
	{
		public static CustomerViewModel ConvertToViewModel(this Customer customer)
		{
			return new CustomerViewModel() { ID = customer.ID };
		}

		public static IEnumerable<CustomerViewModel> ConvertToViewModels(this IEnumerable<Customer> customers)
		{
			var result = new List<CustomerViewModel>();

			foreach (var customer in customers)
			{
				result.Add(customer.ConvertToViewModel());
			}

			return result;
		}

		public static FeatureFlagViewModel ConvertToViewModel(this FeatureFlag featureFlag)
		{
			return new FeatureFlagViewModel() { ID = featureFlag.ID
												, Name = featureFlag.Name };
		}

		public static IEnumerable<FeatureFlagViewModel> ConvertToViewModels(this IEnumerable<FeatureFlag> featureFlags)
		{
			var result = new List<FeatureFlagViewModel>();

			foreach (var featureFlag in featureFlags)
			{
				result.Add(featureFlag.ConvertToViewModel());
			}

			return result;
		}

		public static CustomerFeatureFlagsViewModel ConvertToViewModel(this Domain.Customer.CustomerFeatureFlags customerFeatureFlags)
		{
			var result = new CustomerFeatureFlagsViewModel()
			{
				Customer = customerFeatureFlags.Customer.ConvertToViewModel()
			};

			if (customerFeatureFlags.EnableFeatureFlags != null)
			{
				result.EnableFeatureFlags = customerFeatureFlags.EnableFeatureFlags.ConvertToViewModels();
			}

			return result;
		}

		public static FeatureFlagDetailsViewModel ConvertToViewModel(this FeatureFlagDetails featureFlagDetails)
		{
			var result = new FeatureFlagDetailsViewModel() { ID = featureFlagDetails.ID
																, Name = featureFlagDetails.Name
																, SupportAppVersions = featureFlagDetails.SupportAppVersions };
			if (featureFlagDetails.EnableCustomers != null && featureFlagDetails.EnableCustomers.Any())
			{
				result.EnableCustomers = featureFlagDetails.EnableCustomers.ConvertToViewModels();
			}

			return result;
		}
	}
}
