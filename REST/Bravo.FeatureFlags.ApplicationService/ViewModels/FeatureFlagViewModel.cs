﻿namespace Bravo.FeatureFlags.ApplicationService.ViewModels
{
	using System;

	public class FeatureFlagViewModel
	{
		public Guid ID { get; set; }
		public string Name { get; set; }
	}
}
