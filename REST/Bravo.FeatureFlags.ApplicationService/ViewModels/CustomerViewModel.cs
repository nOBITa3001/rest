﻿namespace Bravo.FeatureFlags.ApplicationService.ViewModels
{
	public class CustomerViewModel
	{
		/// <summary>
		/// Gets or sets customer ID.
		/// </summary>
		public string ID { get; set; }
	}
}
