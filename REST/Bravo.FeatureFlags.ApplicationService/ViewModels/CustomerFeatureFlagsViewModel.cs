﻿namespace Bravo.FeatureFlags.ApplicationService.ViewModels
{
	using System.Collections.Generic;

	public class CustomerFeatureFlagsViewModel
	{
		/// <summary>
		/// Gets or sets customer.
		/// </summary>
		public CustomerViewModel Customer { get; set; }

		/// <summary>
		/// Gets or sets application version.
		/// </summary>
		public string AppVersion { get; set; }

		/// <summary>
		/// Gets or sets list of enable feature flags for specific customer and version.
		/// </summary>
		public IEnumerable<FeatureFlagViewModel> EnableFeatureFlags { get; set; }
	}
}
