﻿namespace Bravo.FeatureFlags.ApplicationService.ViewModels
{
	public class FeatureFlagAuthorizationViewModel
	{
		/// <summary>
		/// Gets or sets customer.
		/// </summary>
		public CustomerViewModel Customer { get; set; }

		/// <summary>
		/// Gets or sets authrized customer flag.
		/// </summary>
		public bool IsAuthorizedCustomer { get; set; }
	}
}
