﻿namespace Bravo.FeatureFlags.ApplicationService.ViewModels
{
	using System.Collections.Generic;

	public class FeatureFlagDetailsViewModel : FeatureFlagViewModel
	{
		#region Properties

		public IEnumerable<string> SupportAppVersions { get; set; }

		public IEnumerable<CustomerViewModel> EnableCustomers { get; set; }

		#endregion
	}
}
