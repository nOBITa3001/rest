﻿namespace Bravo.FeatureFlags.ApplicationService.Messaging
{
	using System;

	public abstract class ServiceResponseBase
	{
		#region Properties

		/// <summary>
		/// Get or sets which api version that is called by clients.
		/// </summary>
		public string APIVersion { get; set; }

		/// <summary>
		/// Save the exception thrown so that consumers can read it.
		/// </summary>
		public Exception Exception { get; set; }

		#endregion
	}
}
