﻿namespace Bravo.FeatureFlags.ApplicationService.Messaging
{
	using System;

	public abstract class IntegerIDRequest : ServiceRequestBase
	{
		private int id;

		public IntegerIDRequest(int id)
		{
			if (id < 1)
			{
				throw new ArgumentException("ID must be positive.");
			}

			this.id = id;
		}

		public int ID
		{
			get
			{
				return this.id;
			}
		}
	}
}
