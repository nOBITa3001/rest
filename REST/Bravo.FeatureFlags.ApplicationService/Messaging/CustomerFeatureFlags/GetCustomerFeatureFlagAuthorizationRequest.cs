﻿namespace Bravo.FeatureFlags.ApplicationService.Messaging.CustomerFeatureFlags
{
	[System.Obsolete()]
	public class GetCustomerFeatureFlagAuthorizationRequest : StringIDRequest
	{
		/// <summary>
		/// Gets an application version.
		/// </summary>
		public string AppVersion { get; private set; }

		/// <summary>
		/// Gets a feature flag name.
		/// </summary>
		public string FeatureFlagName { get; private set; }

		public GetCustomerFeatureFlagAuthorizationRequest(string customerId, string appVersion, string featureFlagName)
			: base(customerId)
		{
			this.AppVersion = appVersion;
			this.FeatureFlagName = featureFlagName;
		}
	}
}
