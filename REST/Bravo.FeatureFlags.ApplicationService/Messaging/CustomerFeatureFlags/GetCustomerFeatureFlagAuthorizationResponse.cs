﻿namespace Bravo.FeatureFlags.ApplicationService.Messaging.CustomerFeatureFlags
{
	[System.Obsolete()]
	public class GetCustomerFeatureFlagAuthorizationResponse : ServiceResponseBase
	{
		public bool IsAuthorizedCustomer { get; set; }
	}
}