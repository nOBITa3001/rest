﻿namespace Bravo.FeatureFlags.ApplicationService.Messaging.CustomerFeatureFlags
{
	using Bravo.FeatureFlags.ApplicationService.ViewModels;

	[System.Obsolete()]
	public class GetCustomerFeatureFlagsResponse : ServiceResponseBase
	{
		public CustomerFeatureFlagsViewModel CustomerFeatureFlags { get; set; }
	}
}
