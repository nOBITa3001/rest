﻿namespace Bravo.FeatureFlags.ApplicationService.Messaging.CustomerFeatureFlags
{
	[System.Obsolete()]
	public class GetCustomerFeatureFlagsRequest : StringIDRequest
	{
		/// <summary>
		/// Gets or sets an application version.
		/// </summary>
		public string AppVersion { get; private set; }

		public GetCustomerFeatureFlagsRequest(string customerId, string appVersion)
			: base(customerId)
		{
			this.AppVersion = appVersion;
		}
	}
}
