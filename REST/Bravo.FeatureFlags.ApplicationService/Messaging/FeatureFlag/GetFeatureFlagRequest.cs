﻿namespace Bravo.FeatureFlags.ApplicationService.Messaging.FeatureFlag
{
	using System;

	public class GetFeatureFlagRequest : GuidIDRequest
	{
		public GetFeatureFlagRequest(Guid id)
			: base(id) { }
	}
}
