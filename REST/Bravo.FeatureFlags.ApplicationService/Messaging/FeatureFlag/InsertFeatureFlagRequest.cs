﻿namespace Bravo.FeatureFlags.ApplicationService.Messaging.FeatureFlag
{
	using System;

	public class InsertFeatureFlagRequest : GuidIDRequest
	{
		public InsertFeatureFlagRequest(Guid id)
			: base(id)
		{
			this.FeatureFlagProperties = new FeatureFlagPropertiesViewModel();
		}

		public FeatureFlagPropertiesViewModel FeatureFlagProperties { get; set; }
	}
}
