﻿namespace Bravo.FeatureFlags.ApplicationService.Messaging.FeatureFlag
{
	using System;

	public class UpdateFeatureFlagRequest : GuidIDRequest
	{
		public UpdateFeatureFlagRequest(Guid id)
			: base(id)
		{
			this.FeatureFlagProperties = new FeatureFlagPropertiesViewModel();
		}

		public FeatureFlagPropertiesViewModel FeatureFlagProperties { get; set; }
	}
}
