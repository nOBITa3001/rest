﻿namespace Bravo.FeatureFlags.ApplicationService.Messaging.FeatureFlag
{
	using Bravo.FeatureFlags.Domain.FeatureFlag;

	public class GetFeatureFlagDetailsResponse : ServiceResponseBase
	{
		public FeatureFlagDetails FeatureFlagDetails { get; set; }
	}
}
