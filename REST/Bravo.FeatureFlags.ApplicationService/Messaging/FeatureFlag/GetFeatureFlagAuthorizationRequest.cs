﻿namespace Bravo.FeatureFlags.ApplicationService.Messaging.FeatureFlag
{
	using System;

	public class GetFeatureFlagAuthorizationRequest : GuidIDRequest
	{
		/// <summary>
		/// Gets a customer ID.
		/// </summary>
		public string CustomerID { get; private set; }

		public GetFeatureFlagAuthorizationRequest(Guid id, string customerId)
			: base(id)
		{
			this.CustomerID = customerId;
		}
	}
}
