﻿namespace Bravo.FeatureFlags.ApplicationService.Messaging.FeatureFlag
{
	using Bravo.FeatureFlags.ApplicationService.ViewModels;
	using System.Collections.Generic;

	public class GetFeatureFlagsResponse : ServiceResponseBase
	{
		public IEnumerable<FeatureFlagViewModel> FeatureFlags { get; set; }
	}
}
