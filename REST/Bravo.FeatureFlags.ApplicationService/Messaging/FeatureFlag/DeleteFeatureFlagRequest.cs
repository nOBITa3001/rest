﻿namespace Bravo.FeatureFlags.ApplicationService.Messaging.FeatureFlag
{
	using System;

	public class DeleteFeatureFlagRequest : GuidIDRequest
	{
		#region Properties

		public FeatureFlagPropertiesViewModel FeatureFlagProperties { get; set; }

		#endregion

		#region Constructures

		public DeleteFeatureFlagRequest(Guid id)
			: base(id)
		{
			this.FeatureFlagProperties = new FeatureFlagPropertiesViewModel();
		}

		#endregion
	}
}
