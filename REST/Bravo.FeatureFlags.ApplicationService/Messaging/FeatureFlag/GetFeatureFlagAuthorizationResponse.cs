﻿namespace Bravo.FeatureFlags.ApplicationService.Messaging.FeatureFlag
{
	using Bravo.FeatureFlags.ApplicationService.ViewModels;

	public class GetFeatureFlagAuthorizationResponse : ServiceResponseBase
	{
		#region Properties

		/// <summary>
		/// Gets or sets feature flag authorization.
		/// </summary>
		public FeatureFlagAuthorizationViewModel FeatureFlagAuthorization { get; set; }

		#endregion

		#region Constructors

		public GetFeatureFlagAuthorizationResponse()
		{
			this.FeatureFlagAuthorization = new FeatureFlagAuthorizationViewModel();
		}

		#endregion
	}
}
