﻿namespace Bravo.FeatureFlags.ApplicationService.Messaging.FeatureFlag
{
	using Domain.FeatureFlag;

	public class GetFeatureFlagResponse : ServiceResponseBase
	{
		public FeatureFlag FeatureFlag { get; set; }
	}
}
