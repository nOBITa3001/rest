﻿namespace Bravo.FeatureFlags.ApplicationService.Messaging.FeatureFlag
{
	using System.Collections.Generic;

	public class FeatureFlagPropertiesViewModel : PropertiesViewModelBase
	{
		public string Name { get; set; }

		public string Description { get; set; }

		public IEnumerable<string> EnableCustomers { get; set; }
	}
}
