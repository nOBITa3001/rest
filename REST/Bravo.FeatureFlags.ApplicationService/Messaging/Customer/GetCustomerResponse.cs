﻿namespace Bravo.FeatureFlags.ApplicationService.Messaging.Customer
{
	using Domain.Customer;

	public class GetCustomerResponse : ServiceResponseBase
	{
		public Customer Customer { get; set; }
	}
}
