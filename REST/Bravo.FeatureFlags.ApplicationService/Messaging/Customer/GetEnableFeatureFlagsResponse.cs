﻿namespace Bravo.FeatureFlags.ApplicationService.Messaging.Customer
{
	using Bravo.FeatureFlags.ApplicationService.ViewModels;

	public class GetEnableFeatureFlagsResponse : ServiceResponseBase
	{
		/// <summary>
		/// Gets or sets customer feature flags.
		/// </summary>
		public CustomerFeatureFlagsViewModel CustomerFeatureFlags { get; set; }
	}
}
