﻿namespace Bravo.FeatureFlags.ApplicationService.Messaging.Customer
{
	public class InsertCustomerRequest : StringIDRequest
	{
		#region Properties

		public CustomerPropertiesViewModel CustomerProperties { get; set; }

		#endregion

		#region Constructures

		public InsertCustomerRequest(string id)
			: base(id)
		{
			this.CustomerProperties = new CustomerPropertiesViewModel();
		}

		#endregion
	}
}
