﻿namespace Bravo.FeatureFlags.ApplicationService.Messaging.Customer
{
	using Bravo.FeatureFlags.Domain.Customer;
	using System.Collections.Generic;

	public class GetCustomersResponse : ServiceResponseBase
	{
		public IEnumerable<Customer> Customers { get; set; }
	}
}
