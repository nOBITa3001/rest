﻿namespace Bravo.FeatureFlags.ApplicationService.Messaging.Customer
{
	using System;
	using System.Collections.Generic;

	public class CustomerPropertiesViewModel : PropertiesViewModelBase
	{
		public IEnumerable<Guid> EnableFeatureFlags { get; set; }
	}
}
