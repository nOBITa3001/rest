﻿namespace Bravo.FeatureFlags.ApplicationService.Messaging.Customer
{
	public class GetCustomerRequest : StringIDRequest
	{
		public GetCustomerRequest(string id)
			: base(id) { }
	}
}
