﻿namespace Bravo.FeatureFlags.ApplicationService.Messaging.Customer
{
	using Bravo.FeatureFlags.Domain.Customer;

	public class GetCustomerDetailsResponse : ServiceResponseBase
	{
		public CustomerDetails CustomerDetails { get; set; }
	}
}
