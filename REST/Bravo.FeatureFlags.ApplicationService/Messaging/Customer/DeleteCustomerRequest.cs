﻿namespace Bravo.FeatureFlags.ApplicationService.Messaging.Customer
{
	public class DeleteCustomerRequest : StringIDRequest
	{
		#region Properties

		public CustomerPropertiesViewModel CustomerProperties { get; set; }

		#endregion

		#region Constructures

		public DeleteCustomerRequest(string id)
			: base(id)
		{
			this.CustomerProperties = new CustomerPropertiesViewModel();
		}

		#endregion
	}
}
