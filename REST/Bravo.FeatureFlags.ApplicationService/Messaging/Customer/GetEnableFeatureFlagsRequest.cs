﻿namespace Bravo.FeatureFlags.ApplicationService.Messaging.Customer
{
	public class GetEnableFeatureFlagsRequest : StringIDRequest
	{
		public GetEnableFeatureFlagsRequest(string customerId)
			: base(customerId) { }
	}
}
