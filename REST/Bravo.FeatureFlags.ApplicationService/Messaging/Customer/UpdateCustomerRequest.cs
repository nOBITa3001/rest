﻿namespace Bravo.FeatureFlags.ApplicationService.Messaging.Customer
{
	public class UpdateCustomerRequest : StringIDRequest
	{
		public UpdateCustomerRequest(string id)
			: base(id)
		{
			this.CustomerProperties = new CustomerPropertiesViewModel();
		}

		public CustomerPropertiesViewModel CustomerProperties { get; set; }
	}
}
