﻿namespace Bravo.FeatureFlags.ApplicationService.Messaging
{
	public abstract class PropertiesViewModelBase
	{
		/// <summary>
		/// Gets or sets record status.
		/// </summary>
		public int RecStatus { get; set; }

		/// <summary>
		/// Gets or sets a request user name.
		/// </summary>
		public string UserName { get; set; }
	}
}
