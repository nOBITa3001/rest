﻿namespace Bravo.FeatureFlags.ApplicationService.Messaging
{
	using System;

	public abstract class GuidIDRequest : ServiceRequestBase
	{
		private Guid id;

		public GuidIDRequest(Guid id)
		{
			if (id == Guid.Empty)
			{
				throw new ArgumentException("ID must has value.");
			}

			this.id = id;
		}

		public Guid ID
		{
			get
			{
				return this.id;
			}
		}
	}
}
