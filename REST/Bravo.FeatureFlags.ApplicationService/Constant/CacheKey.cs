﻿namespace Bravo.FeatureFlags.ApplicationService.Constant
{
	public sealed class CacheKey
	{
		/// <summary>
		/// Enable feature flags cache key.
		/// </summary>
		public const string GET_ENABLE_FEATUREFLAGS = "EnableFeatureFlags";

		/// <summary>
		/// Feature flag authorization cache key.
		/// </summary>
		public const string GET_FEATUREFLAG_AUTHORIZATION = "FeatureFlagAuthorization";

		/// <summary>
		/// Feature flag cache key.
		/// </summary>
		public const string GET_FEATUREFLAG = "FeatureFlag";
	}
}
