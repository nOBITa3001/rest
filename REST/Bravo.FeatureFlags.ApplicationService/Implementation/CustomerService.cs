﻿namespace Bravo.FeatureFlags.ApplicationService.Implementation
{
	using Bravo.FeatureFlags.Domain.FeatureFlag;
	using Bravo.Infrastructure.Common.Caching;
	using Bravo.Infrastructure.Common.Enum;
	using Bravo.Infrastructure.Common.UnitOfWork;
	using Constant;
	using Domain.Customer;
	using Exception;
	using Interface;
	using Messaging.Customer;
	using ModelConversion;
	using System;
	using System.Collections.Generic;
	using System.Data.Entity.Core;
	using System.Linq;
	using System.Text;

	public class CustomerService : ApplicationServiceBase, ICustomerService
	{
		#region Declarations

		private readonly ICustomerRepository customerRepository;
		private readonly ICacheStorage cacheStorage;

		#endregion

		public CustomerService(ICustomerRepository customerRepository, IUnitOfWork unitOfWork, ICacheStorage cacheStorage)
			: base(unitOfWork)
		{
			this.customerRepository = customerRepository;
			this.cacheStorage = cacheStorage;

			this.ThrowExceptionIfServiceIsInvalid();
		}

		#region Implementations

		protected override void ThrowExceptionIfServiceIsInvalid()
		{
			if (this.customerRepository == null)
			{
				throw new ArgumentNullException("Customer repository");
			}

			if (this.cacheStorage == null)
			{
				throw new ArgumentNullException("CacheStorage");
			}
		}

		// TODO: Caching
		public GetCustomerResponse Get(GetCustomerRequest getCustomerRequest)
		{
			var result = new GetCustomerResponse();

			try
			{
				result.Customer = this.customerRepository.Get(getCustomerRequest.ID);
				if (result.Customer == null)
				{
					result.Exception = this.GetCustomerNotFoundException();
				}
			}
			catch (Exception ex)
			{
				result.Exception = ex;
			}

			return result;
		}

		// TODO: Caching
		public GetCustomersResponse GetAll()
		{
			var result = new GetCustomersResponse();

			try
			{
				result.Customers = this.customerRepository.GetAll();
			}
			catch (Exception ex)
			{
				result.Exception = ex;
			}

			return result;
		}

		public InsertCustomerResponse Insert(InsertCustomerRequest insertCustomerRequest)
		{
			var result = new InsertCustomerResponse();

			var newCustomer = this.AssignPropertiesToCustomerDomainType(insertCustomerRequest.CustomerProperties);
			if (newCustomer == null)
			{
				result.Exception = new ArgumentNullException("CustomerProperties in CustomerService");
			}
			else
			{
				this.AssignInsertPropertiesToCustomerDomainType(ref newCustomer, insertCustomerRequest);

				try
				{
					this.ThrowExceptionIfCustomerIsInvalid(newCustomer);

					this.customerRepository.Insert(newCustomer);
					this.UnitOfWork.Commit();
				}
				catch (Exception ex)
				{
					result.Exception = ex;
				}
			}

			return result;
		}

		public UpdateCustomerResponse Update(UpdateCustomerRequest updateCustomerRequest)
		{
			throw new NotImplementedException();
		}

		public UpdateCustomerResponse UpdateCustomerFeatureFlags(UpdateCustomerRequest updateCustomerRequest)
		{
			var result = new UpdateCustomerResponse();

			try
			{
				var existingCustomerFeatureFlags = this.customerRepository.GetCustomerFeatureFlags(updateCustomerRequest.ID);
				if (existingCustomerFeatureFlags != null)
				{
					var assignableProperties = this.AssignAvailablePropertiesToCustomerFeatureFlagsDomain(updateCustomerRequest.CustomerProperties);
					existingCustomerFeatureFlags.EnableFeatureFlags = assignableProperties.EnableFeatureFlags;
					this.ThrowExceptionIfCustomerFeatureFlagsIsInvalid(existingCustomerFeatureFlags);

					this.customerRepository.UpdateCustomerFeatureFlags(existingCustomerFeatureFlags);
					this.UnitOfWork.Commit();
				}
				else
				{
					result.Exception = this.GetStandardCustomerFeatureFlagsNotFoundException();
				}
			}
			catch (Exception ex)
			{
				result.Exception = ex;
			}

			return result;
		}

		public DeleteCustomerResponse Delete(DeleteCustomerRequest deleteCustomerRequest)
		{
			var result = new DeleteCustomerResponse();

			var deletingCustomer = this.AssignPropertiesToCustomerDomainType(deleteCustomerRequest.CustomerProperties);
			if (deletingCustomer == null)
			{
				result.Exception = new ArgumentNullException("CustomerProperties in CustomerService");
			}
			else
			{
				this.AssignDeletePropertiesToCustomerDomainType(ref deletingCustomer, deleteCustomerRequest);

				try
				{
					this.customerRepository.Delete(deletingCustomer);
					this.UnitOfWork.Commit();
				}
				catch (ObjectNotFoundException nex)
				{
					result.Exception = nex;
				}
				catch (Exception ex)
				{
					result.Exception = ex;
				}
			}

			return result;
		}

		public GetEnableFeatureFlagsResponse GetEnableFeatureFlags(GetEnableFeatureFlagsRequest getEnableFeatureFlagsRequest)
		{
			var key = this.GenerateEnableFeatureFlagsCacheKey(getEnableFeatureFlagsRequest.ID);

			var result = this.cacheStorage.Retrieve<GetEnableFeatureFlagsResponse>(key);
			if (result == null)
			{
				double cacheDurationMinutes = 60; //_configurationRepository.GetConfigurationValue<int>("ShortCacheDuration");
				result = this.GetEnableFeatureFlags(getEnableFeatureFlagsRequest.ID);
				if (result.Exception == null)
				{
					this.cacheStorage.Store(key, result, DateTime.UtcNow.AddMinutes(cacheDurationMinutes));
				}
			}

			return result;
		}

		public GetCustomerDetailsResponse GetDetails(GetCustomerRequest getCustomerRequest)
		{
			var result = new GetCustomerDetailsResponse();

			try
			{
				result.CustomerDetails = this.customerRepository.GetDetails(getCustomerRequest.ID);
				if (result.CustomerDetails == null)
				{
					result.Exception = this.GetCustomerNotFoundException();
				}
			}
			catch (Exception ex)
			{
				result.Exception = ex;
			}

			return result;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Generates enable feature flags cache key.
		/// </summary>
		private string GenerateEnableFeatureFlagsCacheKey(string customerId)
		{
			return string.Format("{0}_{1}", CacheKey.GET_ENABLE_FEATUREFLAGS, customerId.ToUpper());
		}

		/// <summary>
		/// Gets enable feature flags from repository.
		/// </summary>
		private GetEnableFeatureFlagsResponse GetEnableFeatureFlags(string customerId)
		{
			var result = new GetEnableFeatureFlagsResponse();
			var customerFeatureFlags = default(CustomerFeatureFlags);

			try
			{
				customerFeatureFlags = this.customerRepository.GetCustomerFeatureFlags(customerId);
				if (customerFeatureFlags == null && customerFeatureFlags.Customer == null)
				{
					result.Exception = this.GetCustomerNotFoundException();
				}
				else
				{
					result.CustomerFeatureFlags = customerFeatureFlags.ConvertToViewModel();
				}
			}
			catch (Exception ex)
			{
				result.Exception = ex;
			}

			return result;
		}

		private void ThrowExceptionIfCustomerIsInvalid(Customer customer)
		{
			var brokenRules = customer.GetBrokenRules();
			if (brokenRules != null && brokenRules.Any())
			{
				var brokenRulesBuilder = new StringBuilder();
				brokenRulesBuilder.AppendLine("There were problems saving the feature flag object:");
				foreach (var brokenRule in brokenRules)
				{
					brokenRulesBuilder.AppendLine(brokenRule.RuleDescription);
				}

				throw new Exception(brokenRulesBuilder.ToString());
			}
		}

		// TODO: remove data validation, let repository handle it.
		private bool IsExistingCustomer(string id)
		{
			return this.customerRepository.Get(id) != null;
		}

		private ResourceNotFoundException GetCustomerNotFoundException()
		{
			return new ResourceNotFoundException("The requested customer was not found.");
		}

		private Customer AssignPropertiesToCustomerDomainType(CustomerPropertiesViewModel properties)
		{
			var result = default(Customer);

			if (properties != null)
			{
				result = new Customer()
				{
					RecStatus = properties.RecStatus
				};
			}

			return result;
		}

		private void AssignInsertPropertiesToCustomerDomainType(ref Customer newCustomer, InsertCustomerRequest insertCustomerRequest)
		{
			if (newCustomer != null && insertCustomerRequest != null)
			{
				newCustomer.ID = insertCustomerRequest.ID;
				newCustomer.RecStatus = (int)RecordStatus.Active;
				newCustomer.RecCreatedBy = insertCustomerRequest.CustomerProperties.UserName;
				newCustomer.RecCreatedWhen = DateTime.Now;
			}
		}

		private void AssignDeletePropertiesToCustomerDomainType(ref Customer deletingCustomer, DeleteCustomerRequest deleteCustomerRequest)
		{
			if (deletingCustomer != null && deleteCustomerRequest != null)
			{
				deletingCustomer.ID = deleteCustomerRequest.ID;
				deletingCustomer.RecDeletedBy = deleteCustomerRequest.CustomerProperties.UserName;
			}
		}

		private CustomerFeatureFlags AssignAvailablePropertiesToCustomerFeatureFlagsDomain(CustomerPropertiesViewModel customerProperties)
		{
			var result = new CustomerFeatureFlags();

			var featureFlags = new List<FeatureFlag>();
			if (customerProperties.EnableFeatureFlags != null && customerProperties.EnableFeatureFlags.Any())
			{
				foreach (var item in customerProperties.EnableFeatureFlags)
				{
					featureFlags.Add(new FeatureFlag() { ID = item });
				}
			}

			result.EnableFeatureFlags = featureFlags;

			return result;
		}

		private void ThrowExceptionIfCustomerFeatureFlagsIsInvalid(CustomerFeatureFlags CustomerFeatureFlags)
		{
			var brokenRules = CustomerFeatureFlags.GetBrokenRules();
			if (brokenRules != null && brokenRules.Any())
			{
				var brokenRulesBuilder = new StringBuilder();
				brokenRulesBuilder.AppendLine("There were problems saving the customer feature flags object:");
				foreach (var brokenRule in brokenRules)
				{
					brokenRulesBuilder.AppendLine(brokenRule.RuleDescription);
				}

				throw new Exception(brokenRulesBuilder.ToString());
			}
		}

		private ResourceNotFoundException GetStandardCustomerFeatureFlagsNotFoundException()
		{
			return new ResourceNotFoundException("The requested customer feature flags was not found.");
		}

		#endregion
	}
}
