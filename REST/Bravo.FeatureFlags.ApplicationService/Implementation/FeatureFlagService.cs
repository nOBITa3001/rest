﻿namespace Bravo.FeatureFlags.ApplicationService.Implementation
{
	using Bravo.Infrastructure.Common.Caching;
	using Bravo.Infrastructure.Common.Enum;
	using Bravo.Infrastructure.Common.UnitOfWork;
	using Constant;
	using Domain.Customer;
	using Domain.FeatureFlag;
	using Exception;
	using Interface;
	using Messaging.FeatureFlag;
	using ModelConversion;
	using System;
	using System.Collections.Generic;
	using System.Data.Entity.Core;
	using System.Linq;
	using System.Text;

	public class FeatureFlagService : ApplicationServiceBase, IFeatureFlagService
	{
		#region Declarations

		private readonly IFeatureFlagRepository featureFlagRepository;
		private readonly ICacheStorage cacheStorage;

		#endregion

		#region Constructors

		public FeatureFlagService(IFeatureFlagRepository featureFlagRepository
									, IUnitOfWork unitOfWork
									, ICacheStorage cacheStorage)
			: base(unitOfWork)
		{
			this.featureFlagRepository = featureFlagRepository;
			this.cacheStorage = cacheStorage;

			this.ThrowExceptionIfServiceIsInvalid();
		}

		#endregion

		#region Implementations

		protected override void ThrowExceptionIfServiceIsInvalid()
		{
			if (this.featureFlagRepository == null)
			{
				throw new ArgumentNullException("FeatureFlag repository");
			}

			if (this.cacheStorage == null)
			{
				throw new ArgumentNullException("CacheStorage");
			}
		}

		//public GetFeatureFlagResponse Get(GetFeatureFlagRequest getFeatureFlagRequest)
		//{
		//	var result = new GetFeatureFlagResponse();

		//	try
		//	{
		//		result.FeatureFlag = this.featureFlagRepository.Get(getFeatureFlagRequest.ID);
		//		if (result.FeatureFlag == null)
		//		{
		//			result.Exception = this.GetStandardFeatureFlagNotFoundException();
		//		}
		//	}
		//	catch (Exception ex)
		//	{
		//		result.Exception = ex;
		//	}

		//	return result;
		//}

		#region Cache example
		/// <summary>
		/// Gets the specified get feature flag request that will look in cache stroage first.
		/// </summary>
		public GetFeatureFlagResponse Get(GetFeatureFlagRequest getFeatureFlagRequest)
		{
			var key = this.GenerateFeatureFlagCacheKey(getFeatureFlagRequest.ID);

			var result = this.cacheStorage.Retrieve<GetFeatureFlagResponse>(key);
			if (result == null)
			{
				double cacheDurationMinutes = 60; //_configurationRepository.GetConfigurationValue<int>("ShortCacheDuration");
				result = this.GetFeatureFlag(getFeatureFlagRequest);
				if (result.Exception == null)
				{
					this.cacheStorage.Store(key, result, DateTime.UtcNow.AddMinutes(cacheDurationMinutes));
				}
			}

			return result;
		}

		/// <summary>
		/// Generates feature flag authorization cache key.
		/// </summary>
		private string GenerateFeatureFlagCacheKey(Guid id)
		{
			return string.Format("{0}_{1}", CacheKey.GET_FEATUREFLAG, id);
		}

		/// <summary>
		/// Gets feature flag from repository.
		/// </summary>
		private GetFeatureFlagResponse GetFeatureFlag(GetFeatureFlagRequest getFeatureFlagRequest)
		{
			var result = new GetFeatureFlagResponse();

			try
			{
				result.FeatureFlag = this.featureFlagRepository.Get(getFeatureFlagRequest.ID);
				if (result.FeatureFlag == null)
				{
					result.Exception = this.GetStandardFeatureFlagNotFoundException();
				}
			}
			catch (Exception ex)
			{
				result.Exception = ex;
			}

			return result;
		}
		#endregion

		public GetFeatureFlagDetailsResponse GetDetails(GetFeatureFlagRequest getFeatureFlagRequest)
		{
			var result = new GetFeatureFlagDetailsResponse();

			try
			{
				result.FeatureFlagDetails = this.featureFlagRepository.GetDetails(getFeatureFlagRequest.ID);
				if (result.FeatureFlagDetails == null)
				{
					result.Exception = this.GetStandardFeatureFlagNotFoundException();
				}
			}
			catch (Exception ex)
			{
				result.Exception = ex;
			}

			return result;
		}

		public GetFeatureFlagsResponse GetAll()
		{
			var result = new GetFeatureFlagsResponse();

			try
			{
				var featureFlags = this.featureFlagRepository.GetAll();
				if (featureFlags != null && featureFlags.Any())
				{
					result.FeatureFlags = featureFlags.ConvertToViewModels();
				}
			}
			catch (Exception ex)
			{
				result.Exception = ex;
			}

			return result;
		}

		public InsertFeatureFlagResponse Insert(InsertFeatureFlagRequest insertFeatureFlagRequest)
		{
			var result = new InsertFeatureFlagResponse();

			var newFeatureFlag = this.AssignPropertiesToFeatureFlagDomainType(insertFeatureFlagRequest.FeatureFlagProperties);
			if (newFeatureFlag == null)
			{
				result.Exception = new ArgumentNullException("FeatureFlagProperties in FeatureFlagService");
			}
			else
			{
				this.AssignInsertPropertiesToFeatureFlagDomainType(ref newFeatureFlag, insertFeatureFlagRequest);

				try
				{
					this.ThrowExceptionIfFeatureFlagIsInvalid(newFeatureFlag);

					this.featureFlagRepository.Insert(newFeatureFlag);
					this.UnitOfWork.Commit();
				}
				catch (Exception ex)
				{
					result.Exception = ex;
				}
			}

			return result;
		}

		public UpdateFeatureFlagResponse Update(UpdateFeatureFlagRequest updateFeatureFlagRequest)
		{
			var result = new UpdateFeatureFlagResponse();

			try
			{
				var updateFeatureFlag = this.AssignPropertiesToFeatureFlagDomainType(updateFeatureFlagRequest.FeatureFlagProperties);
				if (updateFeatureFlag == null)
				{
					throw new ArgumentNullException("FeatureFlagProperties in FeatureFlagService");
				}
				else
				{
					this.AssignUpdatePropertiesToFeatureFlagDomainType(ref updateFeatureFlag, updateFeatureFlagRequest);

					try
					{
						this.featureFlagRepository.Update(updateFeatureFlag);
						this.UnitOfWork.Commit();
					}
					catch (ObjectNotFoundException nex)
					{
						result.Exception = nex;
					}
					catch (Exception ex)
					{
						result.Exception = ex;
					}
				}
			}
			catch (Exception ex)
			{
				result.Exception = ex;
			}

			return result;
		}

		public DeleteFeatureFlagResponse Delete(DeleteFeatureFlagRequest deleteFeatureFlagRequest)
		{
			var result = new DeleteFeatureFlagResponse();

			var deletingFeatureFlag = this.AssignPropertiesToFeatureFlagDomainType(deleteFeatureFlagRequest.FeatureFlagProperties);
			if (deletingFeatureFlag == null)
			{
				result.Exception = new ArgumentNullException("FeatureFlagProperties in FeatureFlagService");
			}
			else
			{
				this.AssignDeletePropertiesToFeatureFlagDomainType(ref deletingFeatureFlag, deleteFeatureFlagRequest);

				try
				{
					this.featureFlagRepository.Delete(deletingFeatureFlag);
					this.UnitOfWork.Commit();
				}
				catch (ObjectNotFoundException nex)
				{
					result.Exception = nex;
				}
				catch (Exception ex)
				{
					result.Exception = ex;
				}
			}

			return result;
		}

		public UpdateFeatureFlagResponse UpdateFeatureFlagCustomers(UpdateFeatureFlagRequest updateFeatureFlagRequest)
		{
			var result = new UpdateFeatureFlagResponse();

			try
			{
				var existingFeatureFlagCustomers = this.featureFlagRepository.GetFeatureFlagCustomers(updateFeatureFlagRequest.ID);
				if (existingFeatureFlagCustomers != null)
				{
					var assignableProperties = this.AssignAvailablePropertiesToFeatureFlagCustomersDomain(updateFeatureFlagRequest.FeatureFlagProperties);
					existingFeatureFlagCustomers.EnableCustomers = assignableProperties.EnableCustomers;
					this.ThrowExceptionIfFeatureFlagCustomersIsInvalid(existingFeatureFlagCustomers);

					this.featureFlagRepository.UpdateFeatureFlagCustomers(existingFeatureFlagCustomers);
					this.UnitOfWork.Commit();
				}
				else
				{
					result.Exception = this.GetStandardFeatureFlagCustomersNotFoundException();
				}
			}
			catch (Exception ex)
			{
				result.Exception = ex;
			}

			return result;
		}

		public GetFeatureFlagAuthorizationResponse GetAuthorization(GetFeatureFlagAuthorizationRequest getFeatureFlagAuthorizationRequest)
		{
			var key = this.GenerateFeatureFlagAuthorizationCacheKey(getFeatureFlagAuthorizationRequest.ID
																	, getFeatureFlagAuthorizationRequest.CustomerID);

			var response = this.cacheStorage.Retrieve<GetFeatureFlagAuthorizationResponse>(key);
			if (response == null)
			{
				double cacheDurationMinutes = 60; //_configurationRepository.GetConfigurationValue<int>("ShortCacheDuration");
				response = this.GetFeatureFlagAuthorization(getFeatureFlagAuthorizationRequest);
				if (response.Exception == null)
				{
					this.cacheStorage.Store(key, response, DateTime.UtcNow.AddMinutes(cacheDurationMinutes));
				}
			}

			return response;
		}

		#endregion

		#region Methods

		private ResourceNotFoundException GetStandardFeatureFlagNotFoundException()
		{
			return new ResourceNotFoundException("The requested feature flag was not found.");
		}

		private ResourceNotFoundException GetStandardFeatureFlagCustomersNotFoundException()
		{
			return new ResourceNotFoundException("The requested feature flag customers was not found.");
		}

		private FeatureFlagCustomers AssignAvailablePropertiesToFeatureFlagCustomersDomain(FeatureFlagPropertiesViewModel featureFlagProperties)
		{
			var result = new FeatureFlagCustomers();

			var customers = new List<Customer>();
			if (featureFlagProperties.EnableCustomers != null && featureFlagProperties.EnableCustomers.Any())
			{
				foreach (var item in featureFlagProperties.EnableCustomers)
				{
					customers.Add(new Customer() { ID = item });
				}
			}

			result.EnableCustomers = customers;

			return result;
		}

		private void ThrowExceptionIfFeatureFlagIsInvalid(FeatureFlag featureFlag)
		{
			var brokenRules = featureFlag.GetBrokenRules();
			if (brokenRules != null && brokenRules.Any())
			{
				var brokenRulesBuilder = new StringBuilder();
				brokenRulesBuilder.AppendLine("There were problems saving the feature flag object:");
				foreach (var brokenRule in brokenRules)
				{
					brokenRulesBuilder.AppendLine(brokenRule.RuleDescription);
				}

				throw new Exception(brokenRulesBuilder.ToString());
			}
		}

		private void ThrowExceptionIfFeatureFlagCustomersIsInvalid(FeatureFlagCustomers featureFlagCustomers)
		{
			var brokenRules = featureFlagCustomers.GetBrokenRules();
			if (brokenRules != null && brokenRules.Any())
			{
				var brokenRulesBuilder = new StringBuilder();
				brokenRulesBuilder.AppendLine("There were problems saving the feature flag customers object:");
				foreach (var brokenRule in brokenRules)
				{
					brokenRulesBuilder.AppendLine(brokenRule.RuleDescription);
				}

				throw new Exception(brokenRulesBuilder.ToString());
			}
		}

		/// <summary>
		/// Generates feature flag authorization cache key.
		/// </summary>
		private string GenerateFeatureFlagAuthorizationCacheKey(Guid id, string customerId)
		{
			return string.Format("{0}_{1}_{2}", CacheKey.GET_FEATUREFLAG_AUTHORIZATION, id, customerId.ToUpper());
		}

		/// <summary>
		/// Gets feature flag authorization from repository.
		/// </summary>
		private GetFeatureFlagAuthorizationResponse GetFeatureFlagAuthorization(GetFeatureFlagAuthorizationRequest getFeatureFlagAuthorizationRequest)
		{
			var result = new GetFeatureFlagAuthorizationResponse();

			try
			{
				result.FeatureFlagAuthorization.Customer = new ViewModels.CustomerViewModel { ID = getFeatureFlagAuthorizationRequest.CustomerID };
				result.FeatureFlagAuthorization.IsAuthorizedCustomer = this.featureFlagRepository.IsAuthorizedCustomer(getFeatureFlagAuthorizationRequest.ID
																														, getFeatureFlagAuthorizationRequest.CustomerID);
			}
			catch (Exception ex)
			{
				result.Exception = ex;
			}

			return result;
		}

		private FeatureFlag AssignPropertiesToFeatureFlagDomainType(FeatureFlagPropertiesViewModel properties)
		{
			var result = default(FeatureFlag);

			if (properties != null)
			{
				result = new FeatureFlag()
				{
					Name = properties.Name
					, Description = properties.Description
					, RecStatus = properties.RecStatus
				};
			}

			return result;
		}

		private void AssignInsertPropertiesToFeatureFlagDomainType(ref FeatureFlag insertFeatureFlag, InsertFeatureFlagRequest insertFeatureFlagRequest)
		{
			if (insertFeatureFlag != null && insertFeatureFlagRequest != null)
			{
				insertFeatureFlag.ID = insertFeatureFlagRequest.ID;
				insertFeatureFlag.RecStatus = (int)RecordStatus.Active;
				insertFeatureFlag.RecCreatedBy = insertFeatureFlagRequest.FeatureFlagProperties.UserName;
				insertFeatureFlag.RecCreatedWhen = DateTime.Now;
			}
		}

		private void AssignUpdatePropertiesToFeatureFlagDomainType(ref FeatureFlag updateFeatureFlag, UpdateFeatureFlagRequest updateFeatureFlagRequest)
		{
			if (updateFeatureFlag != null && updateFeatureFlagRequest != null)
			{
				updateFeatureFlag.ID = updateFeatureFlagRequest.ID;
				updateFeatureFlag.Name = updateFeatureFlagRequest.FeatureFlagProperties.Name;
				updateFeatureFlag.Description = updateFeatureFlagRequest.FeatureFlagProperties.Description;
				updateFeatureFlag.RecModifiedBy = updateFeatureFlagRequest.FeatureFlagProperties.UserName;
				updateFeatureFlag.RecModifiedWhen = DateTime.Now;
			}
		}

		private void AssignDeletePropertiesToFeatureFlagDomainType(ref FeatureFlag deletingFeatureFlag, DeleteFeatureFlagRequest deleteFeatureFlagRequest)
		{
			if (deletingFeatureFlag != null && deleteFeatureFlagRequest != null)
			{
				deletingFeatureFlag.ID = deleteFeatureFlagRequest.ID;
				deletingFeatureFlag.RecDeletedBy = deleteFeatureFlagRequest.FeatureFlagProperties.UserName;
			}
		}

		#endregion
	}
}
