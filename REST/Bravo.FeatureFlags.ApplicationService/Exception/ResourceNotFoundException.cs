﻿namespace Bravo.FeatureFlags.ApplicationService.Exception
{
	public class ResourceNotFoundException : System.Exception
	{
		public ResourceNotFoundException(string message)
			: base(message) { }

		public ResourceNotFoundException()
			: base("The requested resource was not found.") { }
	}
}
